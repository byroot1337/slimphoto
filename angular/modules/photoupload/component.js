(function() {

	"use srict";

	require("style!./style.css");

	module.exports = {
		template: require("html!./template.html"),
		controller: require("./controller"),
		bindings: {}
	};

})();