(function() {

	"use strict";

	var d3 = require("./js/d3");

	/**
	 * @memberof FloorPlan
	 * @ngdoc service
	 * @name FloorPlanInteractionService
	 *
	 * @param {service} ConvertGuidService
	 * @param {service} ContentService
	 * @param {service} ResolveGrossService
	 *
	 * @attr {Boolean} firstZoom
	 * @attr {String} viewportHeightTransform
	 * @attr {String} dwg
	 * @attr {String} plansvg
	 * @attr {integer} zoomdraw
	 * 
	 * @description
	 *   Controller for floorplan component
	 */
	module.exports = ['$rootScope', 'ConvertGuidService', 'ContentService', 'ResolveGrossService', 'FlattDbService', function($rootScope, ConvertGuidService, ContentService, ResolveGrossService, FlattDbService) {

		var o = this;

		/**
		 * What is this? Removing this breaks selectangle
		 * @type {Array}
		 */
		o.currentActive = ContentService.getActiveSpaces();

		return {
			getCurrentActive: getCurrentActive,
			setCurrentActive: setCurrentActive,
			resetCurrentActive: resetCurrentActive,

			highlight: highlight,
			unhighlight: unhighlight,

			removeClassFromSvgElement: removeClassFromSvgElement,
			addClassToSvgElement: addClassToSvgElement,
		};

		/**
		 * Gets the current active from ContentService
		 *
		 * @access   public
		 * @memberof FloorPlanInteractionService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @return   {Array}                   Current active spaces
		 */
		function getCurrentActive() {
			return ContentService.getActiveSpaces();
		}

		/**
		 * Set current active objects
		 *
		 * @access   public
		 * @memberof FloorPlanInteractionService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Object}                   event Javascript event object
		 * @param    {String}                   guid  GUID to evaluate
		 */
		function setCurrentActive(event, guid) {

			if (!guid) {

				ContentService.setActiveSpaces([]);

			} else {

				//////////////////////////////////////////////////////
				// The space is already in the current active array //
				//////////////////////////////////////////////////////
				if (ContentService.getActiveSpaces().indexOf(guid) !== -1) {

					////////////////////////////////////////////////
					// Remove the spaceguid from the active array //
					////////////////////////////////////////////////
					if (event.metaKey || event.ctrlKey) {

						ContentService.setActiveSpaces((ContentService.getActiveSpaces().splice(o.currentActive.indexOf(guid), 1)));

						///////////////////////////////
						// The guid IS the new array //
						///////////////////////////////
					} else if (!event.metaKey && !event.ctrlKey) {

						ContentService.setActiveSpaces([guid]);

					}

					if (ContentService.getActiveSpaces().length === 1 && ContentService.getActiveSpaces()[0] === guid) {
						ContentService.setActiveSpaces([]);
					}

					//////////////////////////////////////////////////////
					// The space is not yet in the current active array //
					//////////////////////////////////////////////////////
				} else {

					///////////////////////
					// Replace the array //
					///////////////////////
					if (!event.metaKey && !event.ctrlKey) {

						ContentService.setActiveSpaces([guid]);

						/////////////////////////////
						// Add or remove the space //
						/////////////////////////////
					} else {

						var newarray = ContentService.getActiveSpaces();
						newarray.push(guid);

						ContentService.setActiveSpaces(newarray);
					}

				}

			}
		}

		/**
		 * Resets the current active array
		 *
		 * @access   public
		 * @memberof FloorPlanInteractionService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-15
		 */
		function resetCurrentActive() {
			o.currentActive = [];
		}

		/**
		 * Highlights the paths for the passed GUID(s)
		 *
		 * @access   public
		 * @memberof FloorPlanInteractionService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String|String[]}              guid GUID(s) to highlight
		 */
		function highlight(guid) {

			if (guid !== undefined && guid !== null) {

				if (typeof guid === "string") {

					highLightItem(guid);

				} else if (typeof guid === "object" && guid.constructor === Array) {

					angular.forEach(guid, function(g) {

						highLightItem(g);

					});
				}
			}
		}

		/**
		 * Unhighlights all paths in the current floorplan
		 *
		 * @access   public
		 * @memberof FloorPlanInteractionService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 */
		function unhighlight() {

		}

		/**
		 * Internal helper for the highlight function
		 *
		 * @access   private
		 * @memberof FloorPlanInteractionService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String}                   guid The GUID to highlight
		 */
		function highLightItem(guid) {

			var pathGuids = angular.element("path[ifcguid='" + guid + "']");

			angular.forEach(pathGuids, function(path) {
				addClassToSvgElement(path, "planPath-hover");
			});

		}

		/**
		 * Internal helper for removing classes from SVG elements which jQuery
		 * does not natively do
		 *
		 * @access   private
		 * @memberof FloorPlanInteractionService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {DOMelement}               element       The DOM element to remove the class from
		 * @param    {String}                   classToRemove The class to remove
		 */
		function removeClassFromSvgElement(element, classToRemove) {
			angular.element(element).attr("class", angular.element(element).attr("class").replace(" " + classToRemove, ""));
		}

		/**
		 * Internal helper for adding classes from SVG elements which jQuery
		 * does not natively do
		 *
		 * @access   private
		 * @memberof FloorPlanInteractionService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {DOMelement}               element       The DOM element to add the class from
		 * @param    {String}                   classToRemove The class to add
		 */
		function addClassToSvgElement(element, classToAdd) {
			if (angular.element(element).attr("class") && angular.element(element).attr("class").indexOf(classToAdd) === -1) {
				angular.element(element).attr("class", angular.element(element).attr("class") + " " + classToAdd);
			}
		}

	}];

})();