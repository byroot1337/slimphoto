(function() {

	"use strict";

	require("angular-file-upload");

	module.exports = ['MODULE_CONFIG', '$cookies', '$scope', 'ContentService', '$mdBottomSheet', 'ConvertGuidService', 'FileUploader', 'IdentityService', 'MessageService', '$mdDialog', '$http', function(MODULE_CONFIG, $cookies, $scope, ContentService, $mdBottomSheet, ConvertGuidService, FileUploader, IdentityService, MessageService, $mdDialog, $http) {

		var o = this;

		$scope.$watch(function() {
			return ContentService.getActiveSpaces();
		}, function(spaces) {
			if (spaces) {
				o.activeSpaces = spaces;

				if (o.activeSpaces.length === 1) {
					ContentService.getSpaces().forEach(function(space) {
						if (space.ifcguid === o.activeSpaces[0]) {
							o.activeSpaceObject = space;
						}
					});
				}

				if (o.activeSpaceObject) {
					showGridBottomSheet(o.activeSpaceObject);
				}

			}
		});

		function showGridBottomSheet(object) {

			$mdBottomSheet.show({
				template: require('html!./add_file_bottom_sheet.html'),
				controller: ["$scope", "$mdBottomSheet", "FileUploader", "$mdDialog", function($scope, $mdBottomSheet, FileUploader, $mdDialog) {

					// var uploader = new FileUploader();
					$scope.uploader = new FileUploader();

					/////////////////////
					// State Variables //
					/////////////////////

					$scope.object = object;
					$scope.convertedGuid = ConvertGuidService.convertGuid(object.ifcguid);

					$scope.buttons = [{
						name: 'Afbeelding',
						accept: "image/*",
						icon: 'file-image',
						filetype: 1,
					}];

					/////////////////////
					// Scope functions //
					/////////////////////

					$scope.listItemClick = function(filetype) {
						angular.element('#document-input-' + $scope.convertedGuid + "-" + filetype).click();
					};

					$scope.uploader.onAfterAddingFile = function(fileItem) {

						// Check if the required variables are here
						if (object && fileItem._file) {

							// Check if the object has a project guid
							if (object.project.ifcguid) {

								// Get the project from cache
								IdentityService.getProjectByGuid(object.project.ifcguid, function(project) {

									// check if the project wass successfully retreived
									if (project) {

										// Cosntruct the formdata object
										var formData = new FormData();
										formData.append("metadata", fileItem._file.name);
										formData.append("type", 1);
										formData.append("visibility", 0);
										formData.append("ifcguid", object.ifcguid);
										formData.append("file", fileItem._file);

										// Upload the file
										IdentityService.uploadProjectFile(project, formData, function(err, data) {
											if (!err) {
												MessageService.showMessage("Bestand met succes toegevoegd.", 3000);
												$mdBottomSheet.hide();
											} else {
												MessageService.showMessage("Er ging iets mis tijdens het uploaden van het bestand.", 3000);
											}
										});
									}

								});
							} else {
								MessageService.showMessage("Fout, meld deze fout bij de support van SlimPhoto.", 3000);
							}

						}
					};

					$scope.addDescription = function() {
						console.log("add description");
					};

					$scope.showPrompt = function(ev) {

						// Appending dialog to document.body to cover sidenav in docs app
						var confirm = $mdDialog.prompt()
							.title(object.description ? 'Pas de beschrijving voor' + object.name + ' aan.' : 'Voeg een beschrijving toe voor' + object.name)
							// .textContent('Bowser is a common name.')
							.placeholder(object.description ? object.description : "Beschrijving")
							.ariaLabel(object.description ? object.description : "Beschrijving")
							.initialValue(object.description ? object.description : "")
							.targetEvent(ev)
							.ok('Voeg toe')
							.cancel('Annuleer');

						$mdDialog.show(confirm).then(function(result) {

							if (result) {
								object.description = result;

								updateSpace(object, "description");
							}

						}, function() {});
					};

					//////////////////////
					// Helper functions //
					//////////////////////

					function isValidExtension(filetype, file) {

						if (file.type) {
							switch (filetype) {
								case 1:
									if (file.type.indexOf("image/") === 0) {
										return true;
									}
									return false;
								case 2:
									if (file.type === "application/pdf") {
										return true;
									}
									break;
								case 3:
									if (file.type === "application/msword" || file.type === "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
										return true;
									}
									break;
								case 4:
									if (file.type === "application/vnd.ms-excel" || file.type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.type === "text/csv") {
										return true;
									}
									break;
								default:
									return false;
							}
						}

					}

					function convertTypeToExtension(filetype) {
						switch (filetype) {
							case 1:
								return "afbeelding";
							case 2:
								return "PDF";
							case 3:
								return "word";
							case 4:
								return "spreadsheet";
							default:
								return "bestand";
						}
					}

				}],
				clickOutsideToClose: true
			});

		}

		function updateSpace(updatedSpace, key) {

			var updateObject = {};
			updateObject[key] = updatedSpace[key];

			$http({
				url: MODULE_CONFIG.baseUrl + "ifc/" + updatedSpace.module.display_name.toLowerCase() + "/update/guid/" + updatedSpace.ifcguid,
				method: "PUT",
				data: updateObject,
				headers: {
					'Authorization': 'Bearer' + $cookies.get("token")
				},
			}).success(function(data) {
				$mdBottomSheet.hide();
				MessageService.showMessage("Ruimteinformatie bijgewerkt", 3000);
			}).error(function(err) {
				MessageService.showMessage("Informatie NIET bijgewerkt, probeer opnieuw", 3000);
				handleError(err);
			});
		}

	}];

})();