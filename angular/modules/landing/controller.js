(function() {

	"use strict";

	module.exports = ['$scope', '$cookies', '$location', '$http', '$timeout', 'MODULE_CONFIG', 'FlattDbService', 'ContentService', 'ConvertGuidService', 'FloorPlanService', 'IdentityService', function($scope, $cookies, $location, $http, $timeout, MODULE_CONFIG, FlattDbService, ContentService, ConvertGuidService, FloorPlanService, IdentityService) {

		var o = this;

		o.$onInit = function() {

			setUser(function() {
				setModules(function() {
					setAllBuildings();
				});
			});

			o.showLanding = true;

		};

		function setUser(callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'user/current',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(userdata) {

				if (userdata) {
					userdata.data.config = JSON.parse(userdata.data.config);
					o.user = userdata.data;

					if (callback) {
						callback();
					}
				}

			}).error(function(err) {
				handleError(err);
			});

		}

		function getUser() {
			return o.user;
		}

		function setModules(callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'identity/modules',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(modules) {

				o.modules = modules.data;

				if (callback) {
					callback(null, modules.data);
				}

			}).error(function(err) {
				if (callback) {
					callback(err, null);
				}
				handleError(err);
			});

		}

		function getModules() {
			return o.modules;
		}

		function setAllBuildings() {

			if (o.modules) {

				var tempBuildings = [];

				o.modules.forEach(function(module) {

					$http({
						method: 'GET',
						url: MODULE_CONFIG.baseUrl + 'ifc/' + module.name.toLowerCase() + '/buildings',
						headers: {
							'Content-Type': "plain/text",
							"Authorization": "Bearer " + $cookies.get("token")
						},
					}).success(function(buildings) {

						o.buildings = buildings;

						o.buildings.forEach(function(building) {

							building.module = module;

							var found = false;

							tempBuildings.forEach(function(tempbuilding) {
								if (tempbuilding.project.ifcguid === building.project.ifcguid) {
									found = true;
								}
							});

							if (!found && building.name) {
								tempBuildings.push(building);
							}
						});

					}).error(function(err) {});

				});

				o.allBuildings = tempBuildings;

			}
		}

		function getAllBuildings() {
			return o.allBuildings;
		}

		o.getStoreys = function() {

			o.selectedBuilding = JSON.parse(o.buildingModel);

			IdentityService.getProjectByGuid(o.selectedBuilding.project.ifcguid, function(project) {

				if (project) {
					o.selectedProject = project;
					resolveFilesForSpaces();
				}

			});

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'ifc/' + o.selectedBuilding.module.name.toLowerCase() + '/storeys',
				params: {
					"building.ifcguid": ConvertGuidService.convertGuidToQueryFriendlyGuid(o.selectedBuilding.ifcguid),
					"getgrosses": false
				},
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(storeys) {
				o.storeys = storeys;

			}).error(function(err) {});

		};

		o.getSpaces = function() {
			o.selectedStorey = JSON.parse(o.storeyModel);

			ContentService.setActiveStorey(o.selectedStorey);

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'ifc/' + o.selectedBuilding.module.name.toLowerCase() + '/spaces',
				params: {
					"storey.ifcguid": ConvertGuidService.convertGuidToQueryFriendlyGuid(o.selectedStorey.ifcguid),
					"getgrosses": false
				},
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(spaces) {
				if (spaces) {

					spaces.forEach(function(space) {
						space.module = o.selectedBuilding.module;
					});

					o.spaces = spaces;
					ContentService.setSpaces(spaces);
					o.showLanding = false;

					resolveFilesForSpaces();
				}

			}).error(function(err) {});
		};

		o.backToSelection = function() {

			o.storeyModel = null;
			o.selectedStorey = null;

			o.buildingModel = null;
			o.selectedBuilding = null;

			o.spaces = null;

			FloorPlanService.resetSVG(false);
			FloorPlanService.setSvgRenderState(false);

			o.showLanding = true;
		};

		function handleError(err) {
			if (err && err.error) {
				if (err.error === "token_expired" || err.error === "token_not_provided") {
					window.location.href = "../identity/login/#?redirect=" + $location.absUrl();
				}
			}
		}

		function resolveFilesForSpaces() {

			var fileMap = {};

			if (o.selectedProject && o.spaces) {

				o.selectedProject.files.forEach(function(file) {
					o.spaces.forEach(function(space) {
						if (file.ifcguid === space.ifcguid && file.metadata !== "headerimage") {

							if (!fileMap[space.ifcguid]) {
								fileMap[space.ifcguid] = {};
								fileMap[space.ifcguid].color = "#F44336";
								fileMap[space.ifcguid].spaces = [];
								fileMap[space.ifcguid].spaces.push(space);
							} else {
								if (fileMap[space.ifcguid].spaces) {
									fileMap[space.ifcguid].spaces.push(file);
								} else {
									fileMap[space.ifcguid].spaces = [];
									fileMap[space.ifcguid].color = "#F44336";
									fileMap[space.ifcguid].spaces.push(file);
								}
							}

						}

						if (space.description) {
							if (!fileMap[space.ifcguid]) {
								fileMap[space.ifcguid] = {};
								fileMap[space.ifcguid].description = true;
							} else {
								fileMap[space.ifcguid].description = true;
							}
						}

					});
				});

				if (FloorPlanService.getSvgRenderState()) {
					colorCircles(fileMap);
				} else {
					$timeout(function() {
						colorCircles(fileMap);
					}, 200);
				}

			}

			function colorCircles(fileMap) {
				for (var key in fileMap) {
					FloorPlanService.setSpaceText(key, {
						"content": (fileMap[key].spaces && fileMap[key].spaces.length) ? fileMap[key].spaces.length : "0",
						"fill": fileMap[key].description ? "#8BC34A" : fileMap[key].color
					});
				}
			}

		}

	}];

})();