onmessage = function(e) {

	var spaces = e.data[0];
	var storey = e.data[1];

	if (spaces.length > 0) {

		var svgstring = "";
		svgstring += "<g rendered guid='" + storey.ifcguid.replace(/\$/g, 'ç') + "' ifcguid='" + storey.ifcguid + "'>";

		var spacetest = [];

		spaces.forEach(function(space) {

			space.guid = space.ifcguid.replace(/\$/g, 'ç');

			if (space.werkplek) {
				spacetest.push(space);
			} else {
				spacetest.unshift(space);
			}
		});

		spaces = spacetest;

		spaces.forEach(function(space) {
			if (space.name && space.name.toLowerCase().indexOf("gross") !== -1) {

				//Gross
				svgstring += "<g class='gross'>";

				svgstring += "<path style='fill: none;' ";
				svgstring += "ng-class='{ " + "dimmed" + " : (currentActive.length > 0) }' "; // weird, but it works
				svgstring += "d='" + space.d + "' ";
				svgstring += "guid='" + space.guid + "' ";
				svgstring += "ifcguid='" + space.ifcguid + "' ";
				svgstring += "name='" + space.name + "' ";
				// svgstring += "ng-hide='getObjectSvg() !== null' ";
				svgstring += ">";

				svgstring += "</path>";
				svgstring += "</g>";

			} else if (!space.name || (space.name !== 'BO' && space.name !== 'BVO' && space.name !== 'GO')) {
				//Non gross
				svgstring += "<g class='state' ";
				svgstring += "ng-mouseenter='highlight($event, \"" + space.ifcguid + "\")' ";
				svgstring += "ng-mouseleave='unhighlight(\"" + space.ifcguid + "\")' ";
				svgstring += "ng-click='clickItems( $event, \"" + space.ifcguid + "\" )'>";

				if (space.ruimtefunctie && (space.ruimtefunctie === "verkeersruimte" || space.ruimtefunctie === "Verkeersruimte")) {
					svgstring += "<path style='fill: rgb(236, 240, 241)' ";
				} else {
					svgstring += "<path style='fill: rgb(255, 255, 255)' ";
				}
				if (space.d) {
					svgstring += "d='" + space.d + "' ";
				}
				svgstring += "ng-class='{\"dimmed\" : (currentActive.length !== 0 && currentActive.indexOf(\"" + space.ifcguid + "\") === -1 && hoveredSpace.indexOf(\"" + space.ifcguid + "\") === -1) , \"selected-guid\" : currentActive.indexOf(\"" + space.ifcguid + "\") !== -1 , \"svg-no-outline\" : DWG, \"planpath-hover\" : hoveredSpace === \"" + space.ifcguid + "\"}' ";
				svgstring += "class='planPath' ";
				svgstring += "guid='" + space.guid + "' ";
				svgstring += "ifcguid='" + space.ifcguid + "' ";
				svgstring += "type='" + space.type + "' ";
				svgstring += "ruimtefunctie='" + space.ruimtefunctie + "' ";
				svgstring += "original_ifcguid='" + space.original_ifcguid + "' ";
				if (space.name) {
					svgstring += "name='" + space.name + "' ";
				}

				svgstring += "data-toggle='tooltip' data-placement='top' title='" + (space.name || "Geen naam") + "'></path>";
				svgstring += "</g>";
			}
		});
		svgstring += "</g>";

		postMessage([svgstring]);
	} else {
		postMessage([""]);
	}

};