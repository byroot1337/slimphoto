(function() {

	"use strict";

	var uuid = require("uuid");

	/**
	 * @memberof Flatt.io
	 * @ngdoc service
	 * @name ConvertGuidService
	 * @description
	 *   This service holds functions that address problems with IFC guids. the most
	 *   common problem is that compressed IFC guids contain $ signs, which are 
	 *   reserved and therefor incompatible with a lot of software implementations
	 */
	module.exports = ['IdentityService', function(IdentityService) {

		var o = this;

		return {
			convertGuid: convertGuid,
			convertGuidToOriginal: convertGuidToOriginal,
			convertGuidToQueryFriendlyGuid: convertGuidToQueryFriendlyGuid,
			isFaultyGuid: isFaultyGuid,
			convertQueryToGuidFriendlyQuery: convertQueryToGuidFriendlyQuery,
			generateGuid: generateGuid,
			generateGuidWithOrganisationId: generateGuidWithOrganisationId,
		};

		/**
		 * Converts the guid to a jquery compatible one by replacing $ with ç
		 * @memberof ConvertGuidService
		 * @param {String} guid the guid to convert
		 * @returns {String} the converted guid
		 */
		function convertGuid(guid) {
			if (guid !== null && guid !== undefined && !Array.isArray(guid)) {
				return guid.replace(/\$/g, 'ç');
			} else if (Array.isArray(guid)) {
				guid.forEach(function(g) {
					g.replace(/\$/g, 'ç');
				});
				return guid;
			}
			return null;
		}

		/**
		 * Converts a guid converted by 'convertGuid' back to its original
		 * @memberof ConvertGuidService
		 * @param {String} guid the guid to convert
		 * @returns {String} the original guid
		 */
		function convertGuidToOriginal(guid) {

			if (guid !== null && guid !== undefined && !Array.isArray(guid)) {
				return guid.replace(/ç/g, '$');
			} else if (Array.isArray(guid)) {
				guid.forEach(function(g) {
					g.replace(/ç/g, '$');
				});
				return guid;
			}

			// console.log(guid);

			// if (guid !== null && guid !== undefined) {
			// 	return guid.replace(/ç/g, '$');
			// }
			return null;
		}

		/**
		 * Converts a guid to a url friendly guid by escaping
		 * all $ characters
		 * @memberof ConvertGuidService
		 * @param {String} guid the guid to convert
		 * @returns {String} the escaped guid
		 */
		function convertGuidToQueryFriendlyGuid(guid) {

			var queryguid = convertGuidToOriginal(guid);

			if (!Array.isArray(guid)) {
				queryguid = queryguid.replace(/\$/g, '\\$');
			} else {
				angular.forEach(guid, function(g) {
					g.replace(/\$/g, '\\$');
				});
			}

			return queryguid;
		}

		/**
		 * Checks whether a guid is faulty, e.g. contains
		 * a dollar sign
		 * @memberof ConvertGuidService
		 * @param {String} guid the guid to check
		 * @returns {Boolean} true if faulty, false if not
		 */
		function isFaultyGuid(guid) {

			if (guid) {
				return guid.indexOf("$") !== -1;
			}
		}

		/**
		 * Converts all common guid fields in a query
		 * object to url friendly queries
		 * @memberof ConvertGuidService
		 * @param {Object} queryobject The query object
		 * @returns {Object} The converted query object
		 */
		function convertQueryToGuidFriendlyQuery(query) {
			for (var key in query) {
				if ((key === "ancestors" ||
						key === "_id" ||
						key === "ifcguid" ||
						key === "space.ifcguid" ||
						key === "space" ||
						key === "storey.ifcguid" ||
						key === "building.ifcguid" ||
						key === "site.ifcguid" ||
						key === "project.ifcguid") && query[key]) {
					query[key] = convertGuidToQueryFriendlyGuid(query[key]);
				}
			}
			return query;
		}

		function isArray(thing) {
			return (Object.prototype.toString.call(thing) === '[object Array]');
		}

		function generateGuid() {
			return getGuid(uuid.v4());
		}

		function generateGuidWithOrganisationId() {
			return IdentityService.getUser().organisation_id + "_" + generateGuid();
		}

		function getGuid(uuid) {
			var guid = getCompressedStringFromGuid(getGuidFromUncompressedString(uuid));
			return guid;
		}

		function compressGuid(string) {
			var uuid = getUncompressedStringFromGuid(getGuidFromCompressedString(string, ""));
			return uuid;
		}

		function getConversionTable() {
			return [
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
				'_', '$'
			];
		}

		function getGuidFromUncompressedString(uuid) {

			var parts = uuid.split("-");

			var uuid = [];

			uuid.Data1 = parseInt(parts[0], 16);
			uuid.Data2 = parseInt(parts[1], 16);
			uuid.Data3 = parseInt(parts[2], 16);

			var temp;
			uuid.Data4 = [];

			temp = parts[3];
			uuid.Data4[0] = parseInt(temp.substring(0, 2), 16);
			uuid.Data4[1] = parseInt(temp.substring(0, 2), 16);

			temp = parts[4];
			uuid.Data4[2] = parseInt(temp.substring(0, 2), 16);
			uuid.Data4[3] = parseInt(temp.substring(2, 4), 16);
			uuid.Data4[4] = parseInt(temp.substring(4, 6), 16);
			uuid.Data4[5] = parseInt(temp.substring(6, 8), 16);
			uuid.Data4[6] = parseInt(temp.substring(8, 10), 16);
			uuid.Data4[7] = parseInt(temp.substring(10, 12), 16);

			return uuid;
		}

		function getCompressedStringFromGuid(guid) {

			var num = [];

			num[0] = (guid.Data1 / 16777216);
			num[1] = (guid.Data1 % 16777216);
			num[2] = (guid.Data2 * 256 + guid.Data3 / 256);
			num[3] = ((guid.Data3 % 256) * 65536 + guid.Data4[0] * 256 + guid.Data4[1]);
			num[4] = (guid.Data4[2] * 65536 + guid.Data4[3] * 256 + guid.Data4[4]);
			num[5] = (guid.Data4[5] * 65536 + guid.Data4[6] * 256 + guid.Data4[7]);

			var string = [
				[],
				[],
				[],
				[],
				[],
				[]
			];

			var result = "";

			var n = 3;
			for (var i = 0; i < 6; i++) {

				if (!cv_to_64(num[i], string[i], n)) {

					return null;
				}
				for (var j = 0; j < string[i].length; j++) {

					if (string[i][j] != '\0') {
						result += string[i][j]
					};

					n = 5;
				}
			}
			return result;
		}

		function cv_to_64(number, code, len) {

			var iDigit;
			var nDigits;
			var result = [];

			if (len > 5) {
				return false;
			}

			nDigits = len - 1;

			for (iDigit = 0; iDigit < nDigits; iDigit++) {
				result[nDigits - iDigit - 1] = getConversionTable()[parseInt(number % 64)];
				number /= 64;
			}

			result[len - 1] = '\0';

			if (parseInt(number) !== 0) {
				return false;
			}

			for (var i = 0; i < result.length; i++) {
				code[i] = result[i];
			}

			return true;
		}

		function cv_from_64(res, str) {

			var index;

			for (var len = 1; len < 5; len++) {
				if (str[len] == '\0') {
					break;
				}
			}

			res[0] = 0;

			for (var i = 0; i < len; i++) {
				index = -1;
				for (var j = 0; j < 64; j++) {
					if (getConversionTable()[j] == str[i]) {
						index = j;
						break;
					}
				}
				if (index == -1) {
					return false;
				}
				res[0] = res[0] * 64 + index;
			}
			return true;
		}

		function getGuidFromCompressedString(string, guid) {

			var str = [
				[],
				[],
				[],
				[],
				[],
				[]
			];


			var num = [
				[],
				[],
				[],
				[],
				[],
				[]
			];

			var len;

			len = string.length;
			if (len != 22) {
				return false;
			}

			var j = 0;
			var m = 2;

			for (var i = 0; i < 6; i++) {
				for (var k = 0; k < m; k++) {
					str[i][k] = string.charAt(j + k);
				}
				str[i][m] = '\0';
				j = j + m;
				m = 4;
			}
			for (var i = 0; i < 6; i++) {
				if (!cv_from_64(num[i], str[i])) {
					return false;
				}
			}

			guid = [];

			guid.Data1 = (num[0][0] * 16777216 + num[1][0]);
			guid.Data2 = (num[2][0] / 256);
			guid.Data3 = ((num[2][0] % 256) * 256 + num[3][0] / 65536);

			guid.Data4 = [];

			guid.Data4[0] = ((num[3][0] / 256) % 256);
			guid.Data4[1] = (num[3][0] % 256);
			guid.Data4[2] = (num[4][0] / 65536);
			guid.Data4[3] = ((num[4][0] / 256) % 256);
			guid.Data4[4] = (num[4][0] % 256);
			guid.Data4[5] = (num[5][0] / 65536);
			guid.Data4[6] = ((num[5][0] / 256) % 256);
			guid.Data4[7] = (num[5][0] % 256);

			return guid;
		}

		function getUncompressedStringFromGuid(guid) {

			var result = "";
			result += guid.Data1.toString(16);
			result += "-";
			result += guid.Data2.toString(16).split(".")[0];
			result += "-";
			result += guid.Data3.toString(16).split(".")[0];
			result += "-";
			result += guid.Data4[0].toString(16).split(".")[0];
			result += guid.Data4[1].toString(16).split(".")[0];
			result += "-";
			result += guid.Data4[2].toString(16).split(".")[0];
			result += guid.Data4[3].toString(16).split(".")[0];
			result += guid.Data4[4].toString(16).split(".")[0];
			result += guid.Data4[5].toString(16).split(".")[0];
			result += guid.Data4[6].toString(16).split(".")[0];
			result += guid.Data4[7].toString(16).split(".")[0];

			return result;
		}

	}];

})();