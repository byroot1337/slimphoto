(function() {

	'use strict';

	module.exports = ["$rootScope", function($rootScope) {

		return {
			subscribe: function(scope, event, callback) {
				var handler = $rootScope.$on(event, callback);
				scope.$on('$destroy', handler);
			},

			notify: function(event) {
				$rootScope.$emit(event);
			}
		};

	}];

})();