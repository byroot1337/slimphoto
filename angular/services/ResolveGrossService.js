(function() {

	"use strict";

	/**
	 * @memberof Flatt.io
	 * @ngdoc service
	 * @name ResolveGrossService
	 * @description
	 *   Has helper fucntions to determine whether a space object is a gross
	 */
	module.exports = [function() {

		return {
			resolveGross: resolveGross,
			resolveStringGross: resolveStringGross,
		};

		/**
		 * Checks if a space object is a gross
		 *
		 * @memberof ResolveGrossService
		 *
		 * @param {Object} space The space object to evaluate
		 * @return {Boolean} True if gross, else false
		 */
		function resolveGross(space) {

			if (space.name && space.name.indexOf("Gross") === -1) {
				if (space.name.indexOf("gross") === -1) {
					if (space.name.indexOf("bouwlaagoppervlakteobject") === -1) {

						return false;

					}
				}
			}

			return true;
		}

		/**
		 * Checks if a space object is a gross
		 *
		 * @memberof ResolveGrossService
		 *
		 * @param {String } validateString The string to evaluate
		 * @return {Boolean} True if gross, else false
		 */
		function resolveStringGross(validateString) {

			if (validateString && validateString !== undefined && validateString.indexOf("Gross") === -1) {
				if (validateString.indexOf("gross") === -1) {
					if (validateString.indexOf("bouwlaagoppervlakteobject") === -1) {

						return false;

					}
				}
			}

			return true;
		}

	}];

})();