var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');

module.exports = {
	context: __dirname + '/angular',
	entry: {
		app: './module.js',
		vendor: [
			// 'angular',
			// 'angular-material',
		]
	},
	output: {
		path: __dirname + '/dist/',
		publicPath: './dist/',
		filename: 'slimphoto.bundle.js'
	},
	styleLoader: require('extract-text-webpack-plugin').extract('style-loader', 'css-loader!postcss-loader!less-loader'),
	module: {
		loaders: [
			{
				test: /\.css$/,
				loader: "style-loader!css-loader",
			}, {
				test: /\.gif$/,
				loader: "file-loader"
			}, {
				test: /\.jpg$/,
				loader: 'url-loader'
			}, {
				test: /\.png$/,
				loader: 'url-loader'
			}, {
				test: /\.worker\.js$/,
				loader: "worker-loader?inline=true"
			}, {
				test: /\.svg$/,
				loader: 'svg-inline'
			}
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			"window.jQuery": "jquery",
			"jQuery": "jquery",
			"$": "jquery",
			"proj4": "proj4",
		}),
		new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.bundle.js"),
		new ExtractTextPlugin("app.bundle.css"),
		new webpack.OldWatchingPlugin(),
	]
};