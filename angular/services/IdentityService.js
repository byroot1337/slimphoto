(function() {

	"use strict";

	/**
	 * @memberof Flatt.io
	 * @ngdoc service
	 * @name IdentityService
	 *
	 * @param {service} $timeout
	 * @param {service} $cookies
	 * @param {service} $http
	 * @param {service} $location
	 * @param {config} MODULE_CONFIG
	 *
	 * @attr {Array} organisationUsers
	 * @attr {Array} groups
	 * @attr {Array} projects
	 * @attr {Array} modules
	 * @attr {Array} organisation
	 * @attr {Array} subscriptions
	 * 
	 * @description
	 *   Handles all identity and authentication related things
	 */
	module.exports = ['$timeout', '$cookies', '$http', '$location', 'MODULE_CONFIG', function($timeout, $cookies, $http, $location, MODULE_CONFIG) {

		var o = this;
		o.organisationUsers = [];
		o.modules = [];
		o.organisation = [];
		o.subscriptions = [];

		resolveUser(function() {
			setGroups();
			setModules();
			setProjects();
		});

		return {
			getUser: getUser,
			setUser: setUser,
			userLogout: userLogout,
			registerUser: registerUser,
			deleteUser: deleteUser,
			updateUser: updateUser,
			inviteUser: inviteUser,

			uploadUserFile: uploadUserFile,

			getOrganisation: getOrganisation,
			setOrganisation: setOrganisation,
			getOrganisationUsers: getOrganisationUsers,
			setOrganisationUsers: setOrganisationUsers,
			createOrganisation: createOrganisation,
			updateOrganisation: updateOrganisation,
			deleteOrganisation: deleteOrganisation,

			getGroups: getGroups,
			setGroups: setGroups,
			createGroup: createGroup,
			updateGroup: updateGroup,
			deleteGroup: deleteGroup,

			getProjects: getProjects,
			getProject: getProject,
			getProjectByGuid: getProjectByGuid,
			getProjectById: getProjectById,
			setProjects: setProjects,

			createProject: createProject,
			updateProject: updateProject,
			deleteProject: deleteProject,

			uploadProjectFile: uploadProjectFile,
			getProjectFile: getProjectFile,

			getFile: getFile,
			deleteFile: deleteFile,
			getFilesForGuid: getFilesForGuid,
			getFilesForObject: getFilesForObject,

			getModules: getModules,
			getModule: getModule,
			setModules: setModules,

			getSubscriptions: getSubscriptions,
			setSubscriptions: setSubscriptions,
			getSubscription: getSubscription,
			createSubscriptions: createSubscriptions,
			updateSubscriptions: updateSubscriptions,
			deleteSubscriptions: deleteSubscriptions,

			importProgress: importProgress,

			// getLocation: getLocation,
			// getTotal: getTotal,
			getOrganisationPrefix: getOrganisationPrefix,
			resolveModulesByKey: resolveModulesByKey,

		};

		/************************************************************
		 * user calls
		 ************************************************************/

		/**
		 * Logs the current user out
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Function} callback 	A callback function containing either 
		 *                              	data or an error
		 */
		function userLogout(callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'user/logout',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(userdata) {
				callback(null, userdata.data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});
		}

		/**
		 * Gets the current user object
		 *
		 * @memberof IdentityService
		 * 
		 * @returns {Object} The current user object
		 */
		function getUser() {
			return o.user;
		}

		/**
		 * Sets the current user
		 *
		 * @memberof IdentityService
		 * 
		 */
		function setUser(callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'user/current',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(userdata) {

				if (userdata) {
					userdata.data.config = JSON.parse(userdata.data.config);
					o.user = userdata.data;

					if (callback) {
						callback();
					}
				}

			}).error(function(err) {
				handleError(err);
			});

		}

		/**
		 * Registers a new user
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	user 		User object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function registerUser(user) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'user/register',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(user),
			}).success(function(userdata) {
				callback(userdata.data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Deletes a user
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Integer} 	id 			the id of the user to delete
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function deleteUser(id, callback) {

			$http({
				method: 'DELETE',
				url: MODULE_CONFIG.baseUrl + 'identity/user/id/' + id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(data) {
				callback(null, data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Updates a user
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	user 		User object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function updateUser(user, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/user/id/' + user.id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(user),
			}).success(function(userdata) {
				callback(null, userdata.data);
				setUser(function(err, data) {

				});
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Invites a new user
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	user 		User object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function inviteUser(user, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/user/invite',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(user)
			}).success(function(data) {
				callback(null, data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Uploads a file to a user
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	user 		User object
		 * @param  {FormData} 	formdata 	formdata object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function uploadUserFile(user, formdata, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/user/id/' + user.id + '/file',
				headers: {
					'Content-Type': undefined,
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: formdata
			}).success(function(data) {
				callback(null, data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/************************************************************
		 * organisation calls
		 ************************************************************/

		/**
		 * Gets the current organisation
		 *
		 * @memberof IdentityService
		 * 
		 * @returns {Object} An organisation object
		 */
		function getOrganisation() {
			return o.organisation;
		}

		/**
		 * Sets the organisation
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	user 		User object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function setOrganisation(user, callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + "identity/organisation/id/" + user.organisation_id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(orgdata) {
				callback(null, orgdata.data);
				o.organisation = orgdata.data;
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Gets users for the current organisation
		 *
		 * @memberof IdentityService
		 * 
		 * @returns {array} Array containing organisation users
		 */
		function getOrganisationUsers() {
			return o.organisationUsers;
		}

		/**
		 * Sets users for an organisation
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	user 		User object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function setOrganisationUsers(user, callback) {

			if (user.level !== 1) {
				$http({
					method: 'GET',
					url: MODULE_CONFIG.baseUrl + "identity/users?organisation_id=" + user.organisation_id,
					headers: {
						'Content-Type': "plain/text",
						"Authorization": "Bearer " + $cookies.get("token")
					},
				}).success(function(orgdata) {
					callback(null, orgdata.data);
					o.organisationUsers = orgdata.data;
				}).error(function(err) {
					callback(err, null);
					handleError(err);
				});
			}

		}

		/**
		 * creates a new organisation
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	organisation 	Organisation object
		 * @param  {Function} 	callback 		A callback function containing either 
		 *                                		data or an error
		 */
		function createOrganisation(organisation, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + "identity/organisation/create",
				headers: {
					"Content-Type": "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(organisation)
			}).success(function(orgdata) {
				callback(null, orgdata.data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Updates an organisation
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	organisation 	Organisation object
		 * @param  {Function} 	callback 		A callback function containing either 
		 *                               		data or an error
		 */
		function updateOrganisation(organisation, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + "identity/organisation/id/" + organisation.id,
				headers: {
					"Content-Type": "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(organisation)
			}).success(function(orgdata) {
				callback(null, orgdata.data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Deletes an organisation
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	user 		User object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function deleteOrganisation(user, callback) {

			$http({
				method: 'DELETE',
				url: MODULE_CONFIG.baseUrl + "identity/organisation/id/" + user.organisation_id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(orgdata) {
				callback(null, orgdata.data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/************************************************************
		 * Group calls
		 ************************************************************/

		/**
		 * Gets the current groups
		 * @returns {Array} Array containing group objects
		 */
		function getGroups() {
			return o.groups;
		}

		/**
		 * Sets the current groups
		 */
		function setGroups(callback) {

			resolveUser(function() {
				$http({
					method: 'GET',
					url: MODULE_CONFIG.baseUrl + "identity/groups?user=" + o.user.id,
					headers: {
						'Content-Type': "plain/text",
						"Authorization": "Bearer " + $cookies.get("token")
					},
				}).success(function(groupdata) {

					o.groups = groupdata.data;

					if (callback) {
						callback();
					}

				}).error(function(err) {
					handleError(err);
				});
			});

		}

		/**
		 * Create a group
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	group 		Group object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function createGroup(group, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/group/create',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(group)
			}).success(function(data) {
				callback(null, data);

				var group = data.data;
				var modules = o.modules.data;
				var projects = o.projects;

				angular.forEach(modules, function(module) {
					module.create = 0;
					module.read = 1;
					module.update = 0;
					module.delete = 0;
				});

				group.modules = modules;

				updateGroup(group, function(err, data) {

				});

				setGroups(function(err, data) {

				});

			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Update a group
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	group 		Group object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function updateGroup(group, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/group/id/' + group.id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(group)
			}).success(function(groupdata) {
				callback(null, groupdata.data);
				setGroups(function(err, data) {

				});
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Delete a group
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	group 		Group object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function deleteGroup(group, callback) {

			$http({
				method: 'DELETE',
				url: MODULE_CONFIG.baseUrl + 'identity/group/id/' + group,
				headers: {
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(data) {
				callback(null, data);
				setGroups(function(err, data) {});
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		// /**
		//  * Sets an active group
		//  * 
		//  * @memberof IdentityService
		//  * 
		//  * @param  {Integer} 	id 			Id of the group
		//  */
		// function setActiveGroup(id) {

		// 	$location.search('groupid', id);

		// 	if (o.groups) {
		// 		resolveAndSetActiveGroupFromCurrentGroups(id);
		// 	} else {
		// 		setGroups(function() {
		// 			resolveAndSetActiveGroupFromCurrentGroups(id);
		// 		});
		// 	}

		// 	o.activeGroupId = id;
		// }

		// /**
		//  * Gets the active group id
		//  * @returns {Integer} the id of the current group
		//  */
		// function getActiveGroupId() {
		// 	if (o.activeGroup) {
		// 		return o.activeGroup.id;
		// 	} else {
		// 		return undefined;
		// 	}
		// }

		// /**
		//  * Gets the active group object
		//  * @returns {Object} The active group object
		//  */
		// function getActiveGroup() {
		// 	return o.activeGroup;
		// }

		/************************************************************
		 * Project calls
		 ************************************************************/

		/**
		 * Gets the current projects
		 * @returns {array} Array containing group objects
		 */
		function getProjects(callback) {
			if (callback) {
				callback();
			}
			return o.projects;
		}

		// function getActiveGroupProjects() {
		// 	return o.activeGroupProjects;
		// }

		/**
		 * Gets a project by id
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Integer} 	id 			Id of the project to get
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function getProject(id, callback) {

			if (o.projects) {

				resolveProjectFromCurrentProjects(id, function(err, project) {
					if (callback) {
						callback(err, project);
					}
				});

			} else {

				setProjects(function(err, data) {

					if (!err) {
						resolveProjectFromCurrentProjects(id, function(err, project) {
							if (callback) {
								callback(err, project);
							}
						});
					} else {
						callback(err);
					}
				});
			}

		}

		/**
		 * Gets a project by guid
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {String} 	guid 		Guid of the project to get
		 */
		function getProjectByGuid(guid, callback) {

			angular.forEach(o.projects, function(project) {
				if (project.guid === guid) {
					callback(project);
				}
			});

		}

		/**
		 * Gets a project by guid
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {String} 	guid 		Guid of the project to get
		 */
		function getProjectById(id, callback) {

			angular.forEach(o.projects, function(project) {
				if (project.id === id) {
					callback(project);
				}
			});

		}

		/**
		 * Sets all projects
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function setProjects(callback) {

			resolveUser(function() {

				$http({
					method: 'GET',
					url: MODULE_CONFIG.baseUrl + "identity/projects?user=" + o.user.id,
					headers: {
						'Content-Type': "plain/text",
						"Authorization": "Bearer " + $cookies.get("token")
					},
				}).success(function(projects) {

					o.projects = projects.data;

					if (callback) {
						callback(null, projects.data);
					}

				}).error(function(err) {
					callback(err, null);
					handleError(err);
				});
			});

		}

		/**
		 * Create a new project
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {FormData} 	formdata 	FormData object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function createProject(formdata, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/project/create',
				headers: {
					'Content-Type': undefined,
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: formdata
			}).success(function(data) {
				callback(null, data);

				setProjects(function(err, data) {
					if (!err) {

					}
				});
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Delete a project
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	project 	project object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function deleteProject(project, callback) {

			$http({
				method: 'DELETE',
				url: MODULE_CONFIG.baseUrl + 'identity/project/id/' + project.id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(project)
			}).success(function(data) {
				callback(null, data);

				setProjects(function(err, data) {
					if (!err) {

					}
				});
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Update a project
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {FormData} 	formdata 	FormData object
		 * @param  {Integer} 	id 			Id of the project to update
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function updateProject(formdata, id, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/project/id/' + id,
				headers: {
					'Content-Type': undefined,
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: formdata
			}).success(function(data) {
				callback(null, data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Uploads a file to a project
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	project 	project object
		 * @param  {FormData} 	formdata 	FormData object
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function uploadProjectFile(project, formdata, callback) {

			//Here we can downsample an image//

			// if(){
			// 	// from an input element
			// 	var filesToUpload = input.files;
			// 	var file = filesToUpload[0];

			// 	var img = document.createElement("img");
			// 	var reader = new FileReader();  
			// 	reader.onload = function(e) {img.src = e.target.result}
			// 	reader.readAsDataURL(file);

			// 	var ctx = canvas.getContext("2d");
			// 	ctx.drawImage(img, 0, 0);

			// 	var MAX_WIDTH = 800;
			// 	var MAX_HEIGHT = 600;
			// 	var width = img.width;
			// 	var height = img.height;

			// 	if (width > height) {
			// 	  if (width > MAX_WIDTH) {
			// 	    height *= MAX_WIDTH / width;
			// 	    width = MAX_WIDTH;
			// 	  }
			// 	} else {
			// 	  if (height > MAX_HEIGHT) {
			// 	    width *= MAX_HEIGHT / height;
			// 	    height = MAX_HEIGHT;
			// 	  }
			// 	}
			// 	canvas.width = width;
			// 	canvas.height = height;
			// 	var ctx = canvas.getContext("2d");
			// 	ctx.drawImage(img, 0, 0, width, height);

			// 	var dataurl = canvas.toDataURL("image/png");

			// 	//Post dataurl to the server with AJAX
			// }

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/project/id/' + project.id + '/file',
				headers: {
					'Content-Type': undefined,
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: formdata
			}).success(function(data) {
				callback(null, data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * get a file for a project
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Object} 	project 	project object
		 * @param  {UUID} 		guid 		guid of the file to get
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function getProjectFile(project, guid, callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'identity/project/id/' + project.id + '/files/ifcguid/' + guid,
				headers: {
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(data) {
				callback(null, data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * get a file
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {String} 	location 	The location of the file to get
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function getFile(location, callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + location,
				responseType: "blob",
				headers: {
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(data) {
				callback(null, data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * get files for guid
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {String} 	location 	The location of the file to get
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function getFilesForGuid(guid, callback) {

			getProjectByGuid(guid, function(projectguid) {

				$http({
					method: 'GET',
					url: MODULE_CONFIG.baseUrl + "identity/project/id/" + projectguid + "/files/ifcguid/" + guid,
					headers: {
						"Authorization": "Bearer " + $cookies.get("token")
					},
				}).success(function(data) {
					callback(null, data);
				}).error(function(err) {
					callback(err, null);
					handleError(err);
				});

			});

		}

		/**
		 * get files for object
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {String} 	location 	The location of the file to get
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function getFilesForObject(object, callback) {

			getProjectByGuid(object.project.ifcguid, function(project) {

				$http({
					method: 'GET',
					url: MODULE_CONFIG.baseUrl + "identity/project/id/" + project.id + "/files/ifcguid/" + object.ifcguid,
					headers: {
						"Authorization": "Bearer " + $cookies.get("token")
					},
				}).success(function(data) {
					callback(null, data);
				}).error(function(err) {
					callback(err, null);
					handleError(err);
				});

			});

		}

		/**
		 * delete a file
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {String} 	location 	The location of the file to get
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function deleteFile(guid, callback) {

			$http({
				method: 'DELETE',
				url: MODULE_CONFIG.baseUrl + "file/delete/" + guid,
				headers: {
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(data) {
				callback(null, data);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/************************************************************
		 * Module calls
		 ************************************************************/

		/**
		 * Gets the current modules
		 *
		 * @memberof IdentityService
		 * 
		 * @returns {Array} Array containing module objects
		 */
		function getModules() {
			return o.modules;
		}

		function getModule(id, callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'identity/modules',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(modules) {

				var found = false;

				for (var i = 0; i < modules.data.length; i++) {
					if (modules.data[i].id === id) {
						found = true;
						callback(null, modules.data[i]);
					}
				}

				if (!found) {
					callback(null, null);
				}

			}).error(function(err) {
				if (callback) {
					callback(err, null);
				}
				handleError(err);
			});

		}

		/**
		 * Sets the current modules
		 *
		 * @memberof IdentityService
		 * 
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function setModules(callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'identity/modules',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(modules) {

				o.modules = modules.data;

				if (callback) {
					callback(null, modules.data);
				}

			}).error(function(err) {
				if (callback) {
					callback(err, null);
				}
				handleError(err);
			});

		}

		/************************************************************
		 * Subscription calls
		 ************************************************************/

		/**
		 * Gets the current subscriptions
		 * @returns {Array} Array containing subscription objects
		 */
		function getSubscriptions() {
			return o.subscriptions;
		}

		/**
		 * Sets the current subscriptions
		 *
		 * @memberof IdentityService
		 *
		 * @param  {Function} 	callback 	A callback function containing either 
		 *                               	data or an error
		 */
		function setSubscriptions(callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'identity/module-subscriptions',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(subscriptions) {
				callback(null, subscriptions);
				o.subscriptions = subscriptions.data;
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Sets the current subscriptions
		 *
		 * @memberof IdentityService
		 *
		 * @param  {Object}		subscription 	A subscription object
		 * @param  {Function} 	callback 		A callback function containing either 
		 *                               		data or an error
		 */
		function getSubscription(subscription, callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'identity/module-subscriptions/id/' + subscription.id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(subscriptions) {
				callback(null, subscriptions);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Create a new subscriptions
		 *
		 * @memberof IdentityService
		 *
		 * @param  {Object}		subscription 	A subscription object
		 * @param  {Function} 	callback 		A callback function containing either 
		 *                               		data or an error
		 */
		function createSubscriptions(subscriptions, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/module-subscription/create',
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(subscriptions)
			}).success(function(subscriptions) {
				callback(null, subscriptions);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Update a subscriptions
		 *
		 * @memberof IdentityService
		 *
		 * @param  {Object}		subscription 	A subscription object
		 * @param  {Function} 	callback 		A callback function containing either 
		 *                               		data or an error
		 */
		function updateSubscriptions(subscriptions, callback) {

			$http({
				method: 'POST',
				url: MODULE_CONFIG.baseUrl + 'identity/module-subscriptions/id/' + subscriptions.id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
				data: JSON.stringify(subscriptions)
			}).success(function(subscriptions) {
				callback(null, subscriptions);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/**
		 * Delete a subscriptions
		 *
		 * @memberof IdentityService
		 *
		 * @param  {Object}		subscription 	A subscription object
		 * @param  {Function} 	callback 		A callback function containing either 
		 *                               		data or an error
		 */
		function deleteSubscriptions(subscriptions, callback) {

			$http({
				method: 'DELETE',
				url: MODULE_CONFIG.baseUrl + 'identity/module-subscriptions/id/' + subscriptions.id,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(subscriptions) {
				callback(null, subscriptions);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/************************************************************
		 * Import calls
		 ************************************************************/

		/**
		 * Gets the progress of a job
		 *
		 *	@memberof IdentityService
		 * 
		 * @param  {UUID}   jobid    	UUID of the job to get the progress for
		 * @param  {Function} 	callback 		A callback function containing either 
		 *                                		data or an error
		 */
		function importProgress(jobid, callback) {

			$http({
				method: 'GET',
				url: MODULE_CONFIG.baseUrl + 'identity/project/parseprogress/' + jobid,
				headers: {
					'Content-Type': "plain/text",
					"Authorization": "Bearer " + $cookies.get("token")
				},
			}).success(function(progress) {
				callback(null, progress);
			}).error(function(err) {
				callback(err, null);
				handleError(err);
			});

		}

		/************************************************************
		 * Data calls
		 ************************************************************/

		// function getLocation(guid, callback) {

		// 	var app = AppService.getCurrentApp();

		// 	$http({
		// 		method: 'GET',
		// 		url: MODULE_CONFIG.baseUrl + '/' + app.display_name.toLowerCase() + '/sites?project.ifcguid=' + guid,
		// 		headers: {
		// 			'Content-Type': "plain/text",
		// 			"Authorization": "Bearer " + $cookies.get("token")
		// 		},
		// 	}).success(function(data) {
		// 		callback(null, data);
		// 	}).error(function(err) {
		// 		callback(err, null);
		// 		handleError(err);
		// 	});
		// }

		// function getTotal(guid, param, callback) {

		// 	var app = AppService.getCurrentApp();

		// 	$http({
		// 		method: 'GET',
		// 		url: MODULE_CONFIG.baseUrl + '/' + app.display_name.toLowerCase() + '/spaces/' + param + '?project.ifcguid=' + guid,
		// 		headers: {
		// 			'Content-Type': "plain/text",
		// 			"Authorization": "Bearer " + $cookies.get("token")
		// 		},
		// 	}).success(function(data) {
		// 		callback(null, data);
		// 	}).error(function(err) {
		// 		callback(err, null);
		// 		handleError(err);
		// 	});

		// }

		/**
		 * Resolves a module by key
		 * @param  {String} key The key to get the module for
		 * @return {Object}     The module
		 */
		function resolveModulesByKey(key) {

			var returnmodules = [];

			angular.forEach(o.modules, function(module) {
				if (module.fields) {
					angular.forEach(module.fields, function(field) {
						if (field === key) {
							returnmodules.push(module);
						}
					});
				}
			});

			return returnmodules;
		}

		/**
		 * Resolves the active group from the current active groups.
		 *
		 * @access   private
		 * @memberof IdentityService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-10-14
		 * @param    {Integer}                   groupid The id of the group to set
		 */
		// function resolveAndSetActiveGroupFromCurrentGroups(groupid) {

		// 	groupid = parseInt(groupid);

		// 	if (o.groups) {

		// 		o.groups.forEach(function(group) {
		// 			if (group.id === groupid) {
		// 				o.activeGroup = group;
		// 			}
		// 		});
		// 	}

		// }

		function resolveProjectFromCurrentProjects(projectid, callback) {
			if (o.projects) {

				var found = false;

				o.projects.forEach(function(project) {
					if (project.id === projectid) {
						found = true;
						callback(null, project);
					}
				});

				if (!found) {
					callback("Not found", null);
				}
			} else {
				callback("no projects", null);
			}
		}

		function resolveUser(callback) {

			if (o.user) {
				callback(o.user);
			} else {
				setUser(function() {
					callback(o.user);
				});
			}
		}

		function getOrganisationPrefix() {
			return (o.user.organisation_id + "_") || "";
		}

		function handleError(err) {
			if (err && err.error) {
				if (err.error === "token_expired" || err.error === "token_not_provided") {
					// window.location.href = "../identity/login/#?redirect=" + $location.absUrl();
				}
			}
		}

	}];

})();