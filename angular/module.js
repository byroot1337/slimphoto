(function() {

	'use strict';

	/**
	 * Vendor
	 */
	var angular = require("angular");
	var _ = require("lodash/core");
	window.jQuery = window.jquery = window.$ = require('jquery');
	var moment = require("moment");

	require("angular-css");
	require("angular-material");
	require("angular-material/angular-material.css");
	require("angular-aria");
	require("angular-sanitize");
	require("angular-animate");
	require("angular-route");
	require("angular-cookies");
	require("angular-translate");
	require("angular-messages");
	require("angular-material-data-table");
	require("ngstorage");
	require("angular-file-upload");
	require("../css/main.css");

	angular.module('SlimPhoto', [

		//Angular deps
		'ngCookies',
		'ngAnimate',
		'ngSanitize',

		//Vendor
		'ngMaterial',
		'ngSanitize',
		'ngStorage',
		// 'openlayers-directive',
		'md.data.table',
		'md.table.templates',
		'angularFileUpload',

		//Modules
		// 'Perfect-Scrollbar',

	]).constant('MODULE_CONFIG', {
		// baseUrl: "https://api.slimlabs.nl/v3/",
		baseUrl: "https://dev.api.flatt.io/v3/",
	}).config(["$provide", "$compileProvider", "$mdIconProvider", "$mdThemingProvider", "$mdDateLocaleProvider", function($provide, $compileProvider, $mdIconProvider, $mdThemingProvider, $mdDateLocaleProvider) {

		//*** This code is copyright 2002-2016 by Gavin Kistner, !@phrogz.net
		//*** It is covered under the license viewable at http://phrogz.net/JS/_ReuseLicense.txt
		//
		// token:   description:             example:
		// yyyy     4-digit year             1999
		// yy       2-digit year             99
		// MMMM     full month name          February
		// MMM      3-letter month name      Feb
		// MM       2-digit month number     02
		// M        month number             2
		// dddd     full weekday name        Wednesday
		// ddd      3-letter weekday name    Wed
		// dd       2-digit day number       09
		// d        day number               9
		// th       day ordinal suffix       nd
		// HHHH     2-digit 24-based hour    17
		// HHH      military/24-based hour   17
		// HH       2-digit hour             05
		// H        hour                     5
		// mm       2-digit minute           07
		// m        minute                   7
		// ss       2-digit second           09
		// s        second                   9
		// ampm     "am" or "pm"             pm
		// AMPM     "AM" or "PM"             PM
		//
		Date.prototype.customFormat = function(formatString) {
			var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhhh, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
			YY = ((YYYY = this.getFullYear()) + "").slice(-2);
			MM = (M = this.getMonth() + 1) < 10 ? ('0' + M) : M;
			MMM = (MMMM = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"][M - 1]).substring(0, 3);
			DD = (D = this.getDate()) < 10 ? ('0' + D) : D;
			DDD = (DDDD = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"][this.getDay()]).substring(0, 3);
			th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) == 1) ? 'st' : (dMod == 2) ? 'nd' : (dMod == 3) ? 'rd' : 'th';
			formatString = formatString.replace("yyyy", YYYY).replace("yy", YY).replace("MMMM", MMMM).replace("MMM", MMM).replace("MM", MM).replace("M", M).replace("DDDD", DDDD).replace("DDD", DDD).replace("DD", DD).replace("D", D).replace("th", th);
			h = (hhh = this.getHours());
			if (h === 0) h = 24;
			if (h > 12) h -= 12;
			hh = h < 10 ? ('0' + h) : h;
			hhhh = hhh < 10 ? ('0' + hhh) : hhh;
			AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
			mm = (m = this.getMinutes()) < 10 ? ('0' + m) : m;
			ss = (s = this.getSeconds()) < 10 ? ('0' + s) : s;
			return formatString.replace("HHHH", hhhh).replace("HHH", hhh).replace("HH", hh).replace("H", h).replace("mm", mm).replace("m", m).replace("ss", ss).replace("s", s).replace("ampm", ampm).replace("AMPM", AMPM);
		};

		$mdThemingProvider.theme('default')
			.primaryPalette('indigo', {
				'default': '500',
			})
			.accentPalette('indigo', {
				'default': '500'
			});

		$mdThemingProvider.definePalette('white', {
			'50': 'ffffff',
			'100': 'ffffff',
			'200': 'ffffff',
			'300': 'ffffff',
			'400': 'ffffff',
			'500': 'ffffff',
			'600': 'ffffff',
			'700': 'ffffff',
			'800': 'ffffff',
			'900': 'ffffff',
			'A100': 'ffffff',
			'A200': 'ffffff',
			'A400': 'ffffff',
			'A700': 'ffffff',
			'contrastDefaultColor': 'dark'
		});

		$mdThemingProvider.theme('dark', 'default')
			.primaryPalette('white')
			.accentPalette('white')
			.dark();

		$mdThemingProvider.enableBrowserColor();

		$mdDateLocaleProvider.formatDate = function(date) {
			return moment(date).format('DD-MM-YYYY');
		};

		$compileProvider.debugInfoEnabled(false);
		$mdIconProvider.defaultIconSet('./icons/mdi.svg');
	}]);

	require("./modules/");
	require("./services/");

})();