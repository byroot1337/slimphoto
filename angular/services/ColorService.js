(function() {

	"use strict";

	/**
	 * @memberof Flatt.io
	 * @ngdoc service
	 * @name ConfigService
	 *
	 * @param {service} $location
	 * @param {service} $timeout
	 * @param {service} SidenavService
	 * 
	 * @description
	 *   Basically handles all config elements. Sets the 
	 *   viewlevel to 'map' if there is no viewlevel in the 
	 *   url hash on startup
	 */
	module.exports = [function() {

		var o = this;

		/**
		 * Color array to pick from
		 *
		 * @type {Array}
		 */
		o.colors = [
			"#F36E21",
			"#76C043",
			"#398BCB",
			"#FBAA19",
			"#37474F",
			"#41B655",
			"#4766B0",
			"#FED600",
			"#25BAA2",
			"#5D4EA0",
			"#AFD136",
			"#00B7D3",
			"#8351A1",

			"#795649",
			"#F8981D",
			"#8BC249",
			"#38A4DD",
			"#903F98",
			"#FEC111",
			"#607D8B",
			"#4DAF4E",
			"#488FCC",
			"#FEEA39",
			"#009688",
			"#4555A5",
			"#CDDC37",
			"#09BCD4",
			"#66499E",

			"#D8CCC8",
			"#FFD181",
			"#CEE18B",
			"#8CAEDC",
			"#FEE580",
			"#D0D8DC",
			"#BEE1C3",
			"#8E99CD",
			"#FAF291",
			"#9CDAE6",
			"#9E89C0",
			"#F0ED84",
			"#85D5F7",
			"#C489BC",
		];

		return {
			setColors: setColors,
			getColors: getColors,
			getColorAtIndex: getColorAtIndex,
		};

		/**
		 * Sets the current color array
		 *
		 * @access   public
		 * @memberof ColorService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-10
		 * @param    {Array}                   colors Array of hex color strings
		 */
		function setColors(colors) {
			o.colors = colors;
		}

		/**
		 * Gets the current color array
		 *
		 * @access   public
		 * @memberof ColorService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-10
		 * @return   {Array}                   Array of hex color strings
		 */
		function getColors() {
			return o.colors;
		}

		/**
		 * Gets a color at a specific index
		 *
		 * @access   public
		 * @memberof ColorService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-10
		 * @param    {Integer}                   index The index of the color
		 * @return   {String}                          Hex color string
		 */
		function getColorAtIndex(index) {
			return o.colors[index];
		}

	}];

})();