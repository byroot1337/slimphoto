(function() {

	"use strict";

	module.exports = ['$rootScope', 'IdentityService', '$location', '$injector', function($rootScope, IdentityService, $location, $injector) {

		var o = this;

		/**
		 * Watches the url hash		for a viewlevel property,
		 * then sets the viewlevel
		 */
		$rootScope.$watch(function() {
			return $location.search().appId;
		}, function() {

			if ($location.search().appId) {
				setCurrentApp(parseInt($location.search().appId));
			}

		}, true);

		/**
		 * Gets the current active group
		 *
		 * @access   private
		 * @memberof PropertyService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-10
		 */
		$rootScope.$watch(function() {
			return IdentityService.getProjects();
		}, function(projects) {
			o.projects = projects;
			resolveRightsPerProject();
		}, true);

		/**
		 * Gets the current modules
		 *
		 * @access   private
		 * @memberof PropertyService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-10
		 */
		$rootScope.$watch(function() {
			return IdentityService.getGroups();
		}, function(groups) {
			o.groups = groups;
			resolveRightsPerProject();
		}, true);

		/****************************************
		 * 			  API EXPOSURE
		 ****************************************/

		return {
			setCurrentApp: setCurrentApp,
			unsetCurrentApp: unsetCurrentApp,
			getCurrentApp: getCurrentApp,

			getModuleConfig: getModuleConfig,
			getModuleConfigs: getModuleConfigs,

			getAppConfig: getAppConfig,
			getCurrentAppConfig: getCurrentAppConfig,

			getAppRightsForProjects: getAppRightsForProjects,
			getAppRightsForProject: getAppRightsForProject,
		};



		function setCurrentApp(id, callback) {

			if ($location.search().appId !== id) {

				$location.search('appId', id);

				IdentityService.getModule(id, function(err, data) {
					if (!err) {

						setActiveAppConfig(getAppConfig(data.display_name.toLowerCase()));

						o.currentApp = data;
						resolveRightsPerProject();

						if (callback) {

							callback(o.currentApp);
						}
					}
				});
			}

		}

		function unsetCurrentApp() {
			$location.search('appId', null);
			o.currentApp = null;
			o.activeAppConfig = null;

		}

		function getCurrentApp() {
			return o.currentApp;
		}

		function getModuleConfig(module) {
			return getModuleConfigs()[module];
		}

		function getModuleConfigs() {
			return {
				"wetregelgeving": {
					icon: "certificate",
					display_name: "Wet en Regelgeving",
				},
				"klimaat": {
					icon: "thermometer-lines",
					display_name: "Wet en Regelgeving",
				},
				"KWIS": {
					icon: "",
					display_name: "Wet en Regelgeving",
				},
				"Werkorders": {
					icon: "worker",
					display_name: "Wet en Regelgeving",
				},
			};
		}

		function setActiveAppConfig(config) {
			if (config) {
				o.activeAppConfig = config;
			}
		}

		function getCurrentAppConfig() {
			return o.activeAppConfig;
		}

		function getAppRightsForProjects() {
			return o.rightsPerProject;
		}

		function getAppRightsForProject(guid) {
			if (o.rightsPerProject) {
				return o.rightsPerProject[guid];
			}
		}

		function resolveRightsPerProject() {

			if (o.currentApp && o.groups && o.projects) {

				var rightsPerProject = {};

				////////////////////////////
				// Loop over the projects //
				////////////////////////////
				for (var i = 0; i < o.projects.length; i++) {

					rightsPerProject[o.projects[i].guid] = {
						create: 0,
						read: 0,
						update: 0,
						delete: 0,
					};

					///////////////////////////
					// Loop over thye groups //
					///////////////////////////
					for (var j = 0; j < o.groups.length; j++) {

						/////////////////////////////////////////////////
						// Check if the group has projects and modules //
						/////////////////////////////////////////////////
						if (o.groups[j].projects && o.groups[j].modules) {

							var projectInGroup = false;
							var groupProject = null;
							var appInGroup = false;
							var groupApp = null;

							//////////////////////////////////////////////////////////
							// check if the current project is in the current group //
							//////////////////////////////////////////////////////////
							for (var k = 0; k < o.groups[j].projects.length; k++) {
								if (o.groups[j].projects[k].id === o.projects[i].id) {
									projectInGroup = true;
									groupProject = o.groups[j].projects[k];
								}
							}

							///////////////////////////////////////////////
							// Check if the current app is in this group //
							///////////////////////////////////////////////
							for (var l = 0; l < o.groups[j].modules.length; l++) {
								if (o.groups[j].modules[l].id === o.currentApp.id) {
									appInGroup = true;
									groupApp = o.groups[j].modules[l];
								}
							}

							/////////////////////////////////////////
							// Determine the rights for this group //
							/////////////////////////////////////////
							if (projectInGroup && groupProject && appInGroup && groupApp) {

								// Loop over the right types
								for (var right in rightsPerProject[o.projects[i].guid]) {

									// Check if the right is not already set to 1
									if (rightsPerProject[o.projects[i].guid][right] !== 1) {

										// Check if the project right and the module right are 1
										if (groupProject[right] === 1 && groupApp[right] === 1) {
											rightsPerProject[o.projects[i].guid][right] = 1;
										}
									}
								}

							}

						}

					}

				}

				o.rightsPerProject = rightsPerProject;
			}

		}

		function getAppConfig(app_name) {

			switch (app_name.toLowerCase()) {

				case "devapp":

					return {
						'viewlevels': {
							'entrylevel': 'map',
							'appPicker': true,
							'dashboard': false,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': true,
							'IfcGroup': false,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': undefined
						},
						'viewlevelTypes': {
							'map': 'IfcBuilding',
							'building': 'IfcSpace',
							'plan': 'IfcSpace',
							'slimtype': undefined
						},
						'viewlevelGrouping': {
							'map': '',
							'building': '',
							'plan': '',
						},
						'apps': {
							'edit_asset': {
								'object': false,
							},
							'dwg': {
								'group': true,
							},
							'kwis': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'climate': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'wetregelgeving': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'property': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'documents': {
								'object': true,
							},
							'assets': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'reviews': {
								'object': false,
							}
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};

				case "wetregelgeving":

					return {
						'viewlevels': {
							'entrylevel': 'dashboard',
							'appPicker': true,
							'dashboard': true,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': false,
							'IfcGroup': true,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': ['lift']
						},
						'viewlevelTypes': {
							'map': 'IfcGroup',
							'building': 'IfcGroup',
							'plan': 'IfcGroup',
							'slimtype': ['lift']
						},
						'viewlevelGrouping': {
							'map': 'verloopdatum',
							'building': 'verloopdatum',
							'plan': 'verloopdatum',
						},
						'viewLevelPanel': {
							'map': 'list',
							'building': 'list',
							'plan': 'list',
						},
						'apps': {
							'edit_asset': {
								'object': false,
							},
							'dwg': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'kwis': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'climate': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'wetregelgeving': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'property': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'documents': {
								'object': false,
							},
							'assets': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'reviews': {
								'object': false,
							}
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};

				case "brandveiligheid":

					return {
						'viewlevels': {
							'entrylevel': 'map',
							'appPicker': true,
							'dashboard': false,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': true,
							'IfcGroup': false,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': undefined
						},
						'viewlevelTypes': {
							'map': 'IfcBuilding',
							'building': 'IfcSpace',
							'plan': 'IfcSpace',
							'slimtype': undefined
						},
						'viewlevelGrouping': {
							'map': '',
							'building': '',
							'plan': '',
						},
						'viewLevelPanel': {
							'map': 'list',
							'building': 'list',
							'plan': 'list',
						},
						'apps': {
							'edit_asset': {
								'object': false,
							},
							'dwg': {
								'group': false,
							},
							'kwis': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'climate': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'wetregelgeving': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'property': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'documents': {
								'object': false,
							},
							'assets': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'reviews': {
								'object': false,
							}
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};

				case "binnenklimaat":

					return {
						'viewlevels': {
							'entrylevel': 'map',
							'appPicker': true,
							'dashboard': false,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': true,
							'IfcGroup': false,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': undefined
						},
						'viewlevelTypes': {
							'map': 'IfcBuilding',
							'building': 'IfcSpace',
							'plan': 'IfcSpace',
							'slimtype': undefined
						},
						'viewlevelGrouping': {
							'map': '',
							'building': '',
							'plan': '',
						},
						'viewLevelPanel': {
							'map': 'list',
							'building': 'apps',
							'plan': 'apps',
						},
						'apps': {
							'edit_asset': {
								'object': false,
							},
							'dwg': {
								'group': false,
							},
							'kwis': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'climate': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'wetregelgeving': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'property': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'documents': {
								'object': false,
							},
							'assets': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'reviews': {
								'object': false,
							}
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};

				case "assetbeheer":

					return {
						'viewlevels': {
							'entrylevel': 'map',
							'appPicker': true,
							'dashboard': false,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': true,
							'IfcGroup': false,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': ["asset"],
						},
						'viewlevelTypes': {
							'map': 'IfcBuilding',
							'building': 'asset',
							'plan': 'asset',
							'slimtype': ["asset"]
						},
						'viewlevelGrouping': {
							'map': '',
							'building': 'nlsfb',
							'plan': 'nlsfb',
						},
						'viewLevelPanel': {
							'map': 'list',
							'building': 'list',
							'plan': 'list',
						},
						'apps': {
							'dwg': {
								'group': false,
							},
							'kwis': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'climate': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'wetregelgeving': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'property': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'documents': {
								'object': false,
							},
							'reviews': {
								'object': false,
							},
							'assets': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'edit_asset': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': true,
							},
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};

				case "kwis":

					return {
						'viewlevels': {
							'entrylevel': 'map',
							'appPicker': true,
							'dashboard': false,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': true,
							'IfcGroup': false,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': undefined
						},
						'viewlevelTypes': {
							'map': 'IfcBuilding',
							'building': 'IfcSpace',
							'plan': 'IfcSpace',
							'slimtype': undefined
						},
						'viewlevelGrouping': {
							'map': '',
							'building': '',
							'plan': '',
						},
						'viewLevelPanel': {
							'map': 'list',
							'building': 'apps',
							'plan': 'apps',
						},
						'apps': {
							'edit_asset': {
								'object': false,
							},
							'dwg': {
								'group': false,
							},
							'kwis': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'climate': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'wetregelgeving': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'property': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'documents': {
								'object': false,
							},
							'assets': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'reviews': {
								'object': false,
							}
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};

				case "verhuur":

					return {
						'viewlevels': {
							'entrylevel': 'map',
							'appPicker': true,
							'dashboard': false,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': true,
							'IfcGroup': false,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': undefined
						},
						'viewlevelTypes': {
							'map': 'IfcBuilding',
							'building': 'IfcSpace',
							'plan': 'IfcSpace',
							'slimtype': undefined
						},
						'viewlevelGrouping': {
							'map': '',
							'building': '',
							'plan': '',
						},
						'viewLevelPanel': {
							'map': 'list',
							'building': 'list',
							'plan': 'list',
						},
						'apps': {
							'edit_asset': {
								'object': false,
							},
							'dwg': {
								'group': true,
							},
							'kwis': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'climate': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'wetregelgeving': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'property': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'documents': {
								'object': true,
							},
							'assets': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'reviews': {
								'object': false,
							}
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};

				case "tekeningenbeheer":

					return {
						'viewlevels': {
							'entrylevel': 'map',
							'appPicker': true,
							'dashboard': false,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': true,
							'IfcGroup': false,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': undefined
						},
						'viewlevelTypes': {
							'map': 'IfcBuilding',
							'building': 'IfcSpace',
							'plan': 'IfcSpace',
							'slimtype': undefined
						},
						'viewlevelGrouping': {
							'map': '',
							'building': '',
							'plan': '',
						},
						'viewLevelPanel': {
							'map': 'list',
							'building': 'apps',
							'plan': 'apps',
						},
						'apps': {
							'edit_asset': {
								'object': false,
							},
							'dwg': {
								'group': true,
							},
							'kwis': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'climate': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'wetregelgeving': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'property': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'documents': {
								'object': false,
							},
							'assets': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'reviews': {
								'object': false,
							}
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};

				default:

					return {
						'viewlevels': {
							'entrylevel': 'map',
							'appPicker': true,
							'dashboard': false,
							'map': true,
							'building': true,
							'plan': true
						},
						'interaction': {
							'BAG': true,
							'IfcSpace': true,
							'IfcGroup': false,
							'IfcObject': true,
							'IfcZone': true,
							'slimtype': undefined,
						},
						'viewlevelTypes': {
							'map': 'IfcBuilding',
							'building': 'IfcSpace',
							'plan': 'IfcSpace',
							'slimtype': undefined
						},
						'viewlevelGrouping': {
							'map': '',
							'building': '',
							'plan': '',
						},
						'viewLevelPanel': {
							'map': 'list',
							'building': 'list',
							'plan': 'list',
						},
						'apps': {
							'edit_asset': {
								'object': false,
							},
							'dwg': {
								'aggregation': true,
								'group': true,
								'multi_object': true,
								'object': true,
							},
							'kwis': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'climate': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'wetregelgeving': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'property': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'documents': {
								'object': true,
							},
							'assets': {
								'aggregation': false,
								'group': false,
								'multi_object': false,
								'object': false,
							},
							'reviews': {
								'object': false,
							}
						},
						'components': {
							'breadcrumbs': true,
							'fabbutton': false,
							'storeynumbers': true,
							'toplistimage': true,
						},
						'modules': {
							'searchbox': false,
						}
					};
			}

		}

	}];

})();