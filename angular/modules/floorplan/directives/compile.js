(function() {

	"use strict";

	/**
	 * @memberof FloorPlan
	 * @ngdoc directive
	 * @name compile
	 *
	 * @param {service} $compile
	 * 
	 * @description
	 *   This directive is used to compile svg strings
	 *   
	 *  @example
	 *
	 * <svg id='planSVG' compile="getSpaceSvg()"></svg>
	 * 
	 */
	module.exports = ['$compile', function($compile) {
		return function(scope, element, attrs) {
			scope.$watch(
				function(scope) {
					// watch the 'compile' expression for changes
					return scope.$eval(attrs.compile);
				},
				function(value) {

					// when the 'compile' expression changes
					// assign it into the current DOM
					element.empty();
					element.append("<svg>" + value + "</svg>");

					// compile the new DOM and link it to the current
					// scope.
					// NOTE: we only compile .childNodes so that
					// we don't get into infinite loop compiling ourselves
					$compile(element.contents())(scope);
				}
			);
		};
    }];

})();