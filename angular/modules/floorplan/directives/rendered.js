(function() {

	"use strict";

	/**
	 * @memberof FloorPlan
	 * @ngdoc directive
	 * @name rendered
	 *
	 * @param {service} $timeout
	 * @param {service} FloorPlanService
	 * 
	 * @description
	 *   This directive is used to track whether and element is rendered
	 *
	 *  @example
	 *
	 * <g rendered></g>
	 * 
	 */
	module.exports = ["$timeout", "FloorPlanService", function($timeout, FloorPlanService) {

		function link(scope) {
			$timeout(function() {
				FloorPlanService.setSvgRenderState(true);
			}, 0);
		}

		return {
			link: link
		};

    }];

})();