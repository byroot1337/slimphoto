(function() {

	"use strict";

	/**
	 * @memberof Flatt.io
	 * @ngdoc service
	 * @name ContentService
	 *
	 * @param {service} $rootScope
	 * @param {service} ConvertGuidService
	 * @param {service} FlattDbService
	 * @param {service} ResolveGrossService
	 * @param {config} MODULE_CONFIG
	 * @param {service} $location
	 * @param {service} IdentityService
	 *
	 * @attr {Array} buildings
	 * @attr {Array} storeys
	 * @attr {Array} spaces
	 * @attr {Array} activeSpaces
	 * @attr {Array} objects
	 * 
	 * @description
	 *   Takes care of all content related state variables.
	 */
	module.exports = [

		'$rootScope',
		'$timeout',
		'ConvertGuidService',
		'FlattDbService',
		'ResolveGrossService',
		'MODULE_CONFIG',
		'$location',
		'IdentityService',
		'ColorService',
		'AppService',
		'MessageService',
		'$timeout',

		function($rootScope, $timeout, ConvertGuidService, FlattDbService, ResolveGrossService, MODULE_CONFIG, $location, IdentityService, ColorService, AppService, MessageService) {

			/****************************
			 * 		State variables
			 ***************************/

			var o = this;

			o.buildings = [];
			o.storeys = [];
			o.spaces = [];
			$rootScope.$emit("ContentService.getSpaces", o.spaces);
			o.activeSpaces = [];
			o.activeAssets = [];
			o.objects = [];

			/****************************
			 * 			API
			 ***************************/

			return {

				// Spaces
				setSpaces: setSpaces,
				getSpaces: getSpaces,
				setActiveSpaces: setActiveSpaces,
				getActiveSpaces: getActiveSpaces,
				getActiveSpaceObject: getActiveSpaceObject,
				setHoveredSpaces: setHoveredSpaces,
				unsetHoveredSpaces: unsetHoveredSpaces,
				getHoveredSpaces: getHoveredSpaces,

				setActiveStorey: setActiveStorey,
				getActiveStorey: getActiveStorey,

				// Active
				resetAllActiveObjects: resetAllActiveObjects,
				resetAllActive: resetAllActive,

			};

			/**
			 * Sets the active storey object by guid
			 * @memberof ContentService
			 * @param {String} guid The guid of the active storey to set
			 */
			function setActiveStorey(storey) {

				o.activeStorey = storey;

			}

			/**
			 * Gets the active storey object
			 * @memberof ContentService
			 * @returns {Object} The active storey object
			 */
			function getActiveStorey() {
				return o.activeStorey;
			}

			/*************************
					SPACES
			*************************/

			/**
			 * Set spaces
			 * @memberof ContentService
			 * @param {Object} query An object representing a query
			 */
			function setSpaces(spaces) {
				o.spaces = spaces;
			}

			/**
			 * Gets the spaces array
			 * @memberof ContentService
			 * @returns {Array} An array containing space objects
			 */
			function getSpaces() {
				return o.spaces;
			}

			/**
			 * Sets the active spaces guid array
			 * @memberof ContentService
			 * @param {Array} guids An array containing the 
			 *                              guids of the spaces to 
			 *                              set active
			 */
			function setActiveSpaces(guids) {

				if (!getActiveSpaces() || !arraysAreEqual(getActiveSpaces(), guids)) {

					$timeout(function() {

						o.activeSpaces = guids;

						if (!guids) {
							o.activeSpaces = [];
							setActiveSpaceObject(null);
						} else {

							if (guids.length === 1) {
								setActiveSpaceObject(guids[0]);
							} else {
								setActiveSpaceObject(null);
							}
						}
					});

				}

			}

			/**
			 * Gets the active spaces array
			 * @memberof ContentService
			 * @returns {Array} Array containing space objects
			 */
			function getActiveSpaces() {
				return o.activeSpaces;
			}

			/**
			 * Sets the active space object
			 * @memberof ContentService
			 * @param {String} guid the guid to set the space for
			 */
			function setActiveSpaceObject(guid) {

			}

			/**
			 * Gets the active space object
			 * @memberof ContentService
			 * @returns {Object} A space object
			 */
			function getActiveSpaceObject() {
				return o.activeSpaceObject;
			}

			/**
			 * Sets the hovered space guids
			 * @memberof ContentService
			 * @param {Array} guids An array containing space guids
			 */
			function setHoveredSpaces(guids) {

				if (o.allowedInteraction && o.allowedInteraction.IfcSpace === true) {
					o.hoveredSpaces = guids;
				} else if (o.allowedInteraction && o.allowedInteraction.IfcGroup === true) {

					var tempGuids;

					if (angular.isArray(guids)) {

						if (guids.length === 1) {
							tempGuids = guids[0];
							resolveSpaceInteraction(tempGuids);
						} else {

							var tempArray = [];

							guids.forEach(function(guid) {
								getSpaceFromCache(guid, function(space) {
									if (space && space.grouped_by) {

										if (o.allowedInteraction.slimtype) {
											getGroupFromCache(space.grouped_by[0], function(group) {
												if (o.allowedInteraction.slimtype.indexOf(group.slimtype.toLowerCase()) !== -1) {
													if (tempArray.indexOf(space.ifcguid) === -1) {
														tempArray.push(space.ifcguid);
													}
												}
											});
										} else {
											if (tempArray.indexOf(space.ifcguid) === -1) {
												tempArray.push(space.ifcguid);
											}
										}

									}
								});
							});

							o.hoveredSpaces = tempArray;
						}

					} else {
						tempGuids = guids;
						resolveSpaceInteraction(tempGuids);
					}

					function resolveSpaceInteraction(guid) {

						getSpaceFromCache(guid, function(space) {
							if (space.grouped_by) {

								if (o.allowedInteraction.slimtype) {
									getGroupFromCache(space.grouped_by[0], function(group) {
										if (o.allowedInteraction.slimtype.indexOf(group.slimtype.toLowerCase()) !== -1) {
											o.hoveredSpaces = group.subelementguids;
										}
									});
								} else {
									o.hoveredSpaces = space.ifcguid;
								}

							}
						});

					}

				}

			}

			/**
			 * Unsets the current hovered spaces array
			 * @memberof ContentService
			 */
			function unsetHoveredSpaces() {
				o.hoveredSpaces = [];
			}

			/**
			 * Get the hovered space guids
			 * @memberof ContentService
			 * @returns {Array} An array containing space guids
			 */
			function getHoveredSpaces() {
				return o.hoveredSpaces;
			}
			/**
			 * Adds a single space to the current collection
			 * @memberof ContentService
			 * @param {String} guid The guid of the space to add
			 */
			function addSingleSpace(guid) {

				FlattDbService.getByGuid(guid, function(data) {

					o.spaces = data;
					$rootScope.$emit("ContentService.getSpaces", o.spaces);

					//o.spaces.unshift(data);
					calculateArea(o.spaces);
					return data;
				});

			}

			function resetAllActiveObjects() {

				// Reset all hovered
				unsetAllHovered();

				// reset all objects
				setActiveSpaces([]);
				setActiveSpaceObject(null);
				setActiveAssets([]);
				setActiveAssetObject(null);
				setActiveObject(null);
				setActiveGroupingObject(null);
				setBagPandInformationForBuilding(null);
			}

			function resetAllActive() {

				resetAllActiveObjects();

				// Reset all others too
				setActiveStorey(null);
				setActiveBuildings(null);
				setActiveBuildingObject(null);
				setActiveProject(null);
			}

			/*************************
					HELPERS
			*************************/



			/**
			 * Retreives elements for object grouping object
			 *
			 * @access   private
			 * @memberof ContentService
			 * @author Wouter Coebergh <wco@byroot.nl>
			 * @date     2016-10-21
			 * @param    {Object}                   group    The object grouping to get the subelements for
			 * @param    {Function}                 callback Callback to run when algorithm is complete
			 * @return   {Object}                            As callback argument: the enriched group with new
			 *                                               subelements array
			 */
			function enrichGroupByElements(group, callback, addSubs) {

				if (group) {

					var tempgroup = angular.copy(group);

					var query = {
						"grouped_by": tempgroup.ifcguid
					};

					tempgroup.subelementcount = 0;

					if (!addSubs) {

						FlattDbService.calculateAreaForSpaces(query, function(data) {
							if (data) {
								tempgroup.area = data.area;
								tempgroup.subelementcount = data.count;
							}
							FlattDbService.distinctSimpleQuery("ifcguid", query, function(data) {
								tempgroup.subelementguids = data;
								callback(tempgroup);
							});
						});

					} else {

						if (!tempgroup.subelements) {
							tempgroup.subelements = [];
						}

						FlattDbService.countData("objects", query, function(data) {

							if (data.count && data.count === tempgroup.subelements.length) {
								callback(tempgroup);
							} else {

								var uniqueBuilding = true;
								var lastBuildingGuid = null;
								var uniqueStorey = true;
								var lastStoreyGuid = null;

								FlattDbService.getData("IfcSpace", query, function(data) {

									tempgroup.area = 0;

									for (var i = 0; i < data.length; i++) {

										// Add to subelements array
										tempgroup.subelements.push(data[i]);

										// Add to area
										if (data[i].area) {
											tempgroup.area += data[i].area;
										}

										// Check if building is unique for group
										if (!lastBuildingGuid) {
											lastBuildingGuid = data[i].building.ifcguid;
										} else {
											if (lastBuildingGuid !== data[i].building.ifcguid) {
												uniqueBuilding = false;
											}
										}

										// Check if storey is unique for group
										if (!lastStoreyGuid) {
											lastStoreyGuid = data[i].storey.ifcguid;
										} else {
											if (lastStoreyGuid !== data[i].storey.ifcguid) {
												uniqueStorey = false;
											}
										}

									}

									if (data.length > 0 && uniqueBuilding) {
										tempgroup.building = angular.copy(data[0].building);
									}

									if (data.length > 0 && uniqueStorey) {
										tempgroup.storey = angular.copy(data[0].storey);
									}

									callback(tempgroup);

								});

							}
						});
					}

				} else {
					callback(null);
				}

			}

			function arraysAreEqual(array1, array2) {

				if (!array2) {
					return false;
				}

				// compare lengths - can save a lot of time 
				if (array1.length != array2.length) {
					return false;
				}

				for (var i = 0, l = array1.length; i < l; i++) {
					// Check if we have nested arrays
					if (array1[i] instanceof Array && array2[i] instanceof Array) {
						// recurse into the nested arrays
						if (!array1[i].equals(array2[i]))
							return false;
					} else if (array1[i] != array2[i]) {
						// Warning - two different object instances will never be equal: {x:20} != {x:20}
						return false;
					}
				}
				return true;

			}

		}
	];

})();