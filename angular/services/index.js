(function() {

	'use strict';

	angular.module("SlimPhoto").service("IdentityService", require("./IdentityService"));
	angular.module("SlimPhoto").service("ContentService", require("./ContentService"));
	angular.module("SlimPhoto").service("FlattDbService", require("./FlattDbService"));
	angular.module("SlimPhoto").service("ConvertGuidService", require("./ConvertGuidService"));
	angular.module("SlimPhoto").service("MessageService", require("./MessageService"));
	angular.module("SlimPhoto").service("AppService", require("./AppService"));
	angular.module("SlimPhoto").service("ResolveGrossService", require("./ResolveGrossService"));
	angular.module("SlimPhoto").service("ColorService", require("./ColorService"));
	angular.module("SlimPhoto").service("SlimPhotoService", require("./SlimPhotoService"));

})();