(function() {

	"use strict";

	var SvgSpaceWorker = require("./workers/spacesvg.worker.js");
	var SvgObjectWorker = require("./workers/objectsvg.worker.js");
	var SvgAssetWorker = require("./workers/assetsvg.worker.js");
	var d3 = require("./js/d3");

	require("./tooltip/bootstrap.min.js");
	require("./tooltip/bootstrap.min.css");

	/**
	 * @memberof FloorPlan
	 * @ngdoc controller
	 * @name FloorPlanController
	 *
	 * @param {service} $timeout
	 * @param {service} $scope
	 * @param {service} $window
	 * @param {service} $sce
	 * @param {config}  MODULE_CONFIG
	 * @param {service} FlattDbService
	 * @param {service} ContentService
	 * @param {service} ResolveGrossService
	 * @param {service} FloorPlanInteractionService
	 * @param {service} FloorPlanService
	 *
	 * @attr {Boolean} firstZoom
	 * @attr {String} viewportHeightTransform
	 * @attr {String} dwg
	 * @attr {String} plansvg
	 * @attr {integer} zoomdraw
	 * 
	 * @description
	 *   Controller for floorplan component
	 */
	module.exports = ['$scope', '$timeout', '$window', '$sce', 'FlattDbService', 'ContentService', 'ResolveGrossService', 'FloorPlanInteractionService', 'FloorPlanService', 'ConvertGuidService', function($scope, $timeout, $window, $sce, FlattDbService, ContentService, ResolveGrossService, FloorPlanInteractionService, FloorPlanService, ConvertGuidService) {

		var o = this;

		/************************************
						STATES
		************************************/

		o.posX = 0;
		o.posY = 0;
		o.newX = 0;
		o.newY = 0;
		o.initLength = 0;
		o.currentScale = 1;
		o.panzoomSelector = '#panzoomEl';
		o.lastmousedown = {};

		var tenArr = [];
		for (var i = 0; i < 100; i++) {
			tenArr.push(i);
		}
		o.gridTenMetersArray = tenArr;

		var oneArr = [];
		for (var j = 0; j < 1000; j++) {
			oneArr.push(j);
		}
		o.gridMetersArray = oneArr;

		/**
		 * The zoom increment value
		 *
		 * @type {Number}
		 */
		o.zoomIncrement = 2;

		/**
		 * The minumum zoom scale of the floorplan
		 *
		 * @type {Number}
		 */
		o.minScale = 2;

		/**
		 * The maximum scale of the floorplan
		 *
		 * @type {Number}
		 */
		o.maxScale = 200;

		/**
		 * Whether there is a first zoom or not
		 *
		 * @type {Boolean}
		 */
		o.firstZoom = true;

		/**
		 * The viewport height transform attribute
		 *
		 * @type {String}
		 */
		o.viewportHeightTransform = "translate(50," + (angular.element(window).innerHeight() - 120) + ")";

		/**
		 * How many times there has been zoomed
		 *
		 * @type {Number}
		 */
		o.zoomdraw = 0;

		/**
		 * global tooltip options
		 *
		 * @type {Object}
		 */
		o.tooltipOptions = {
			container: "body",
			placement: "top",
			trigger: "hover",
			animation: true
		};

		/**
		 * The current active items
		 *
		 * @type {Array}
		 */
		$scope.currentActive = [];

		/**
		 * The current active items
		 *
		 * @type {Array}
		 */
		$scope.currentActive = [];



		/************************************
						INIT
		************************************/

		/////////////////////////////////
		// Use PanZoom to pan and zoom //
		/////////////////////////////////
		// var $section = angular.element('#contentdiv');
		var $panzoom = angular.element(o.panzoomSelector).panzoom({
			rangeStep: 5,
			increment: o.zoomIncrement,
			transition: false,
			minScale: o.minScale,
			maxScale: o.maxScale
		});

		/////////////////////////
		// Add event listeners //
		/////////////////////////
		$panzoom.on('mousewheel.focus', function(e) {
			e.preventDefault();
			zoomSvg(e, $panzoom);
		});
		$panzoom.on('DOMMouseScroll', function(e) {
			e.preventDefault();
			zoomSvg(e, $panzoom);
		});

		document.getElementById("svg").addEventListener('touchmove', touchHandler, false);
		document.getElementById("svg").addEventListener('touchstart', setStartLine, false);
		document.getElementById("svg").addEventListener('touchend', setCurrentScale, false);

		// Touch
		document.getElementById("floorplan-backdrop-rect").addEventListener('touchstart', function(ev) {
			o.clickDown(ev);
		}, false);

		document.getElementById("floorplan-backdrop-rect").addEventListener('touchend', function(ev) {
			o.clickUp(ev);
		}, false);

		document.getElementById("svgContainer").addEventListener("touchmove", function(ev) {
			$scope.moveEditingElement(ev);
		}, false);

		document.getElementById("svgholder").addEventListener("touchend", function(ev) {
			$scope.deselectEditingElement(ev);
		}, false);

		// Chrome pointer events
		document.getElementById("floorplan-backdrop-rect").addEventListener('pointerdown', function(ev) {
			o.clickDown(ev);
		}, false);

		document.getElementById("floorplan-backdrop-rect").addEventListener('pointerup', function(ev) {
			o.clickUp(ev);
		}, false);

		// Normal mouse events
		document.getElementById("floorplan-backdrop-rect").addEventListener('mousedown', function(ev) {
			o.clickDown(ev);
		}, false);

		document.getElementById("floorplan-backdrop-rect").addEventListener('mouseup', function(ev) {
			o.clickUp(ev);
		}, false);

		/************************************
					WATCHERS
		************************************/


		$scope.$watch(ContentService.getSpaces, function(spaces) {
			o.spaces = spaces;
			setSpacePlan(spaces);

		});

		$scope.$watch(ContentService.getActiveStorey, function(storey) {
			o.activeStorey = storey;
		});

		$scope.$watch(ContentService.getActiveSpaces, function(currentActive) {

			$scope.currentActive = currentActive;

			if (currentActive.length === 0) {
				FloorPlanInteractionService.unhighlight();
			}

		}, true);

		/************************************
					FUNCTIONS
		************************************/

		/**
		 * Handles the window resize for this component
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 */
		$window.onresize = function() {
			if (window.innerWidth > 799) {
				angular.element("#svg").css("height", (window.innerHeight - 50));
			} else {
				angular.element("#svg").css("height", (window.innerHeight * 0.6));
			}

			if (o.activeStorey) {
				FloorPlanService.lookAtObject(o.activeStorey.ifcguid);
			}

		};

		/**
		 * Function that runs when a floorplan spacepath is clicked
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Object}                   $event Angular event object
		 * @param    {String}                   guid   GUID that is being clicked
		 */
		$scope.clickItems = function($event, guid) {

			FloorPlanInteractionService.setCurrentActive($event, guid);

		};

		/**
		 * Gets the current plansvg attribute
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @return   {String}                   The floorplan svg string for the spaces
		 */
		$scope.getSpaceSvg = function() {
			return o.plansvg;
		};

		/**
		 * Resolves whether the space is a gross space or not
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-07
		 * @param    {Object}                   space Database space object
		 * @return   {Boolean}                        True is gross, False if nit
		 */
		$scope.resolveGross = function(space) {
			return ResolveGrossService.resolveGross(space);
		};

		/**
		 * Resolves whether a space is a traffic area space or not
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-07
		 * @param    {Object}                   space Database space object
		 * @return   {Boolean}                        True if traffic area, false if not
		 */
		$scope.resolveTrafficArea = function(space) {
			if (space.ruimtefunctie.toLowerCase().indexOf("verkeersruimte") !== -1) {
				return true;
			}
			return false;
		};

		/**
		 * Sets the hovered spaces attribute in the ContentService
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Object}                   $event Angular event object
		 * @param    {String|String[]}          guid   GUID(s) that are hovered
		 */
		$scope.highlight = function($event, guid) {
			if (!angular.element(o.panzoomSelector).panzoom("isDisabled")) {
				ContentService.setHoveredSpaces(guid);
			}
		};

		/**
		 * Unsets all hovered spaces in the ContentService
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String}                   guid GUID(s) to unset
		 */
		$scope.unhighlight = function() {
			if (!angular.element(o.panzoomSelector).panzoom("isDisabled")) {
				ContentService.unsetHoveredSpaces();
			}
		};

		/**
		 * Handles different click events
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Object}                   $event Angular event object
		 */
		o.clickDown = function(event) {

			// Set the single touch location as base attributes of the event
			if (event.touches && event.touches.length === 1) {
				event.clientX = event.touches[0].clientX;
				event.clientY = event.touches[0].clientY;
			}

			if (event.type === "mousedown" || event.type === "pointerdown" || event.type === "touchstart") {

				if (event.currentTarget.id === "floorplan-backdrop-rect" || event.target.id === "floorplan-backdrop-rect") {
					o.lastmousedown.x = event.clientX;
					o.lastmousedown.y = event.clientY;
				}

			}

		};

		o.clickUp = function(event) {

			// Set the single touch location as base attributes of the event
			if (event.changedTouches && event.changedTouches.length === 1) {
				event.clientX = event.changedTouches[0].clientX;
				event.clientY = event.changedTouches[0].clientY;
			}

		};

		/************************************
				  HELPER FUNCTIONS
		************************************/


		function setSpacePlan(spaces) {

			if (spaces) {

				o.plansvg = null;

				if (spaces.length > 0) {

					// Make a checkable property to determine if the 
					// async process is still the last one
					var spacePlanTime = new Date().getTime();
					o.spacePlanTime = spacePlanTime;

					// Initialze the space worker
					var svgspaceworker = new SvgSpaceWorker();

					// Catch the response of the worker
					svgspaceworker.onmessage = function(e) {

						if (spacePlanTime === o.spacePlanTime) {

							o.plansvg = e.data[0];

							$timeout(function() {});

							$timeout(function() {

								var elements = document.getElementsByClassName("planPath");

								for (var i = 0; i < elements.length; i++) {
									elements[i].addEventListener("touchstart", function(ev) {
										$scope.clickItems(ev, this.attributes.ifcguid.value);
									}, false);
								}

								angular.element(".planPath").tooltip(o.tooltipOptions);


								if (o.firstZoom) {
									autoScale(o.activeStorey.ifcguid);
								}

							});

						}

						colorSpaces();
						svgspaceworker.terminate();
					};

					// Post message to the svg worker
					svgspaceworker.postMessage([spaces, o.activeStorey]);

				} else {
					$timeout(function() {
						o.plansvg = null;
					});
				}

			}

		}

		/**
		 * Autoscales the current plan, waits for a plan to render
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 */
		function autoScale(guid) {

			///////////////////////////////////////////////////
			//Check if plan is rendered, otherwise wait 50ms //
			///////////////////////////////////////////////////
			if (!document.getElementById("planSVG") || document.getElementById("planSVG").getBoundingClientRect().width === 0 || !document.getElementById("planSVG").children[0].children[0]) {

				setTimeout(function() {
					autoScale(guid);
				}, 100);

			} else {

				if (o.firstZoom) {

					o.firstZoom = false;

					angular.element("#svg").css("height", window.innerHeight - 50); // 50 = navbar height

					FloorPlanService.lookAtObject(guid);

					if (angular.element(o.panzoomSelector).panzoom("getMatrix") !== undefined) {
						FloorPlanService.setInitPosition(angular.element(o.panzoomSelector).panzoom("getMatrix").input);
					}

				}
			}
		}

		/**
		 * Touch event interaction handler
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Object}                   e Javascript event object
		 */
		function touchHandler(e) {

			var oldX = o.posX;
			var oldY = o.posY;

			var matrix = angular.element(o.panzoomSelector).panzoom("getMatrix");

			/////////////////////////////
			//Dragging with one finger //
			/////////////////////////////
			if (e.touches.length === 1) {

				if ((e.clientX) && (e.clientY)) {
					o.posX = e.clientX;
					o.posY = e.clientY;
				} else if (e.targetTouches) {
					o.posX = e.targetTouches[0].clientX;
					o.posY = e.targetTouches[0].clientY;
					e.preventDefault();
				}

				o.newX = oldX - o.posX;
				o.newY = oldY - o.posY;

				if (o.newX > 75 || o.newY > 75 || o.newX < -75 || o.newY < -75) { // 75 precision tolerance

				} else {
					angular.element(o.panzoomSelector).panzoom("pan", parseFloat(matrix[4]) - parseFloat(o.newX), parseFloat(matrix[5]) - parseFloat(o.newY));
				}

				//////////////////////////
				// dragging and zooming //
				//////////////////////////
			} else if (e.touches.length === 2) {

				var currentLength;

				/////////
				//Drag //
				/////////
				if ((e.clientX) && (e.clientY)) {

					o.posX = e.clientX;
					o.posY = e.clientY;

				} else if (e.touches) {

					o.posX = ((e.touches[0].clientX + e.touches[1].clientX) / 2);
					o.posY = ((e.touches[0].clientY + e.touches[1].clientY) / 2);

					// Calculate length of the line
					currentLength = (Math.sqrt(Math.pow(e.touches[1].clientX - e.touches[0].clientX, 2) + Math.pow(e.touches[1].clientY - e.touches[0].clientY, 2)));

					e.preventDefault();
				}

				o.newX = oldX - o.posX;
				o.newY = oldY - o.posY;

				var focalObj = {
					clientX: parseFloat(o.posX),
					clientY: parseFloat(o.posY)
				};

				if (o.newX > 75 || o.newY > 75 || o.newX < -75 || o.newY < -75) {
					////////////////////////////////
					// Do Nothing, withing marges //
					////////////////////////////////
				} else {
					angular.element(o.panzoomSelector).panzoom("pan", parseFloat(matrix[4]) - parseFloat(o.newX), parseFloat(matrix[5]) - parseFloat(o.newY));
				}

				/////////
				//Zoom //
				/////////
				angular.element(o.panzoomSelector).panzoom("zoom", ((currentLength / o.initLength) * o.currentScale), {
					focal: focalObj,
					maxScale: o.maxScale,
					minScale: o.minScale
				});

			} else {
				//////////////////////////
				//more than two touches //
				//////////////////////////
			}

		}

		/**
		 * Sets the start line for touch interaction
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Object}                   e Javscript event object
		 */
		function setStartLine(e) {
			if (e.touches.length === 2) {
				o.initLength = (Math.sqrt(Math.pow(e.touches[1].clientX - e.touches[0].clientX, 2) + Math.pow(e.touches[1].clientY - e.touches[0].clientY, 2)));
			}
		}

		/**
		 * Sets the currentscale by the panzoomelement matrix
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 */
		function setCurrentScale() {
			o.currentScale = angular.element(o.panzoomSelector).panzoom("getMatrix")[0];
		}

		/**
		 * Zooms the floorplan using panzoom and an event.
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Object}                   e        Javscript event object
		 * @param    {Object}                   $panzoom PanZoom object
		 */
		function zoomSvg(e, $panzoom) {

			e.preventDefault();

			var delta = e.delta || e.originalEvent.wheelDelta;
			if (!delta) {
				delta = -1 * e.originalEvent.detail;
			}

			var scale = 1.1;

			if (delta ? delta < 0 : e.originalEvent.deltaY > 0) {
				scale = 1 / scale;
			}

			$panzoom.panzoom('zoom', (scale * angular.element(o.panzoomSelector).panzoom("getMatrix")[0]), {
				focal: e,
			});

		}

		/**
		 * Colors spaces for the current listitems?
		 *
		 * @todo Refactor this? This seems weird, should be in the floorplanservice
		 *
		 * @access   private
		 * @memberof FloorPlanController
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 */
		function colorSpaces() {

			if (o.viewLevel === 'plan' && o.state === true) {

				FloorPlanService.resetSVG(true);

				angular.forEach(o.listitems, function(items) {

					var color = items.color;

					var tempArray = [];

					getSubElements(tempArray, items).forEach(function(ifcguid) {
						FloorPlanService.setFill(ifcguid, color);
					});

				});
			}
		}

		function getSvgElementByGuid(guid) {
			return angular.element("[guid='" + ConvertGuidService.convertGuid(guid) + "']");
		}

		function getSubElements(arr, item) {

			if (item.subelements) {

				for (var i = 0; i < item.subelements.length; i++) {

					if (!item.subelements[i].subelements && item.subelements[i].ifcguid) {

						arr.push(item.subelements[i].ifcguid);

					} else if (item.subelements[i].subelements) {

						arr = getSubElements(arr, item.subelements[i]);

					}

				}

			}

			return arr;

		}

	}];

})();