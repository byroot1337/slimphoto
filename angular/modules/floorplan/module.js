(function() {

	"use strict";

	require("./js/panzoom");
	require("./js/limit");
	require("./js/pathseg");

	/**
	 * @memberof Flatt.io
	 * @ngdoc module
	 * @name FloorPlan
	 * 
	 * @description
	 *   FloorPlan module
	 */
	// angular.module('FloorPlan', []);
	angular.module('SlimPhoto').service('FloorPlanService', require('./service'));
	angular.module('SlimPhoto').service('FloorPlanInteractionService', require('./interaction-service'));
	angular.module('SlimPhoto').component('slimFloorPlan', require('./component'));

	angular.module('SlimPhoto').directive('compile', require('./directives/compile'));
	angular.module('SlimPhoto').directive('rendered', require('./directives/rendered'));

})();