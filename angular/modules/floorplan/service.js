(function() {

	"use strict";

	var d3 = require("./js/d3");
	var pointInSvgPolygon = require("point-in-svg-polygon");

	/**
	 * @memberof FloorPlan
	 * @ngdoc service
	 * @name FloorPlanService
	 *
 * @param {		service
	}
	ConvertGuidService
	 * @param {service} ContentService
	 *
	 * @attr {Boolean} defaults
	 * @attr {String} renderstate
	 * 
	 * @description
	 *   Controller for floorplan component
	 */
	module.exports = ['ConvertGuidService', 'ContentService', '$timeout', function(ConvertGuidService, ContentService, $timeout) {

		var o = this;
		o.defaults = {
			"opacity": 0.2,
			"stroke-width": 0.05,
			"color": "black",
		};
		o.renderstate = false;
		o.panzoomSelector = '#panzoomEl';

		/**
		 * Show grid or not
		 *
		 * @type {Boolean}
		 */
		o.showGrid = false;

		return {
			setFill: setFill,
			setStroke: setStroke,
			setStrokeWidth: setStrokeWidth,
			setSpaceText: setSpaceText,
			resetSpaceText: resetSpaceText,
			setAllTransparent: setAllTransparent,

			resetSVG: resetSVG,

			setInitPosition: setInitPosition,

			setSvgRenderState: setSvgRenderState,
			getSvgRenderState: getSvgRenderState,

			getShowGrid: getShowGrid,
			setShowGrid: setShowGrid,
			toggleShowGrid: toggleShowGrid,

			getZoom: getZoom,

			centerObject: centerObject,
			lookAtObject: lookAtObject,
			getCenterOfObject: getCenterOfObject,
			setTransformElement: setTransformElement,
			pathIsContainedInPath: pathIsContainedInPath,

			setOnlyInformation: setOnlyInformation,
			getOnlyInformation: getOnlyInformation,
		};

		/******************************
				API FUNCTIONS
		******************************/

		/**
		 * Sets the fill for the passed GUID(s)
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String|String[]}          guids the GUID(s) to set the fill for
		 * @param    {String}                   color HEX or RGB color
		 */
		function setFill(guids, color) {

			if (checkType(guids) === "Array") {
				guids.forEach(function(guid) {
					setPathFill(guid, color);
				});
			} else {
				setPathFill(guids, color);
			}
		}

		/**
		 * Sets the stroke for the passed GUID(s)
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String|String[]}          guids the GUID(s) to set the stroke for
		 * @param    {String}                   color HEX or RGB color
		 */
		function setStroke(guids, color) {
			if (checkType(guids) === "Array") {
				guids.forEach(function(guid) {
					setPathStroke(guid, color);
				});
			} else {
				setPathStroke(guid, color);
			}
		}

		/**
		 * Sets the stroke width for the passed GUID(s)
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String|String[]}          guids the GUID(s) to set the fill for
		 * @param    {Integer}                  width The width of the stroke
		 */
		function setStrokeWidth(guids, width) {
			if (checkType(guids) === "Array") {
				guids.forEach(function(guid) {
					setPathStrokeWidth(guid, width);
				});
			} else {
				setPathStrokeWidth(guid, width);
			}
		}

		/**
		 * Sets text for the specified guid
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String}                   guid   The guid to set text for
		 * @param    {String}                   text   The text to set
		 * @param    {Boolean}                  circle Put it in a circle yes or no
		 */
		function setSpaceText(guid, options) {

			if (o.renderstate) {
				if (options.content) {

					var content = options.content;
					var textColor = options.textColor || "#000";
					var radius = options.radius || 0.5;
					var fill = options.fill || "#F44336";
					var fillOpacity = options.fillOpacity || 0.6;
					var stroke = options.stroke || "#000";
					var strokeWidth = options.strokeWidth || 0;

					// guid = ConvertGuidService.convertGuid(guid);

					angular.forEach(angular.element("path[ifcguid='" + guid + "']"), function(path) {

						if (path !== null) {

							var lineData = [];

							for (var i = 0; i < path.pathSegList.numberOfItems; i++) {
								lineData.push([path.pathSegList.getItem(i).x, path.pathSegList.getItem(i).y]);
							}

							var area = d3.geom.polygon(lineData);
							var centroid = area.centroid();

							var group = d3.select("#svgContainer").append("g")
								.attr("transform", "translate(0 " + (2 * centroid[1]) + ") scale(1 -1)")
								.attr("class", "space_text space_text " + guid);

							group.append("circle")
								.attr("cx", centroid[0])
								.attr("cy", centroid[1] - 0.15)
								.attr("r", radius)
								.attr("stroke", stroke)
								.attr("stroke-width", strokeWidth)
								.style({
									"fill": fill,
									"opacity": fillOpacity
								});

							var text = group.append("text")
								.attr("font-family", "sans-serif")
								.attr("font-size", "0.5px")
								.attr("text-anchor", "middle");

							text.append("tspan")
								.attr("x", centroid[0])
								.attr("y", centroid[1])
								.text(String(content))
								.attr("fill", textColor);
						}
					});


				} else {
					console.warn("options.content is required");
				}
			} else {
				$timeout(function() {
					setSpaceText(guid, options);
				}, 500);
			}



		}

		function resetSpaceText() {
			angular.element("g.space_text").remove();
		}

		/**
		 * Sets all paths in the floorplan transparent
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 */
		function setAllTransparent() {

			angular.forEach(angular.element("#planSVG").find("path"), function(path) {

				addClassToSvgElement(path, "dimmed");

			});

		}

		/**
		 * Resets the SVG to its initial state
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Boolean}			resetposition Whether to also set the plan back to its initial position
		 */
		function resetSVG(resetposition) {

			angular.element("g.space_text").remove();

			angular.forEach(angular.element("#planSVG").find("path"), function(path) {

				if (angular.element(path).attr("ruimtefunctie") && angular.element(path).attr("ruimtefunctie").toLowerCase().indexOf("verkeersruimte") !== -1) {
					angular.element(path).css({
						fill: 'rgb(236, 240, 241)',
					});
				} else if (angular.element(path).attr("name") && angular.element(path).attr("name").toLowerCase().indexOf("gross") !== -1) {
					angular.element(path).css({
						fill: 'none',
					});
				} else {
					angular.element(path).css({
						fill: 'rgb(255, 255, 255)',
					});
				}

			});

			angular.forEach(angular.element("#assetSVG").find("path"), function(path) {
				angular.element(path).css({
					fill: 'rgb(255, 255, 255)',
				});
			});

			if (resetposition) {
				angular.element(o.panzoomSelector).attr("transform", getInitPosition());
			}

		}

		/**
		 * Set the initial position of the floorplan
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Object}			initPos Position object
		 */
		function setInitPosition(initPos) {
			o.initPosition = initPos;
		}

		/**
		 * Gets the initial position of the floorplan
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @return   {Object}			Position object
		 */
		function getInitPosition() {
			return o.initPosition;
		}

		/**
		 * Sets the renderstate
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {Boolean}                   boolean The state of rendering
		 */
		function setSvgRenderState(boolean) {
			o.renderstate = boolean;
		}

		/**
		 * Gets the render state
		 *
		 * @access   public
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @return   {Boolean}                   The renderstate
		 */
		function getSvgRenderState() {
			return o.renderstate;
		}

		function getShowGrid() {
			return o.showGrid;
		}

		function setShowGrid(bool) {
			o.showGrid = bool;
		}

		function toggleShowGrid() {
			o.showGrid = !o.showGrid;
		}

		function getZoom() {
			return parseFloat(angular.element(o.panzoomSelector).attr("transform").split("(")[1].split(",")[0]);
		}

		function centerObject(guid, callback) {

			var paths = getSvgElementByGuid(guid);

			var success = false;

			if (paths.length === 1) {

				var innerRect = paths[0].getBoundingClientRect();
				var outerRect = document.getElementById("svg").getBoundingClientRect();

				var svgCenter = {
					x: (outerRect.right - outerRect.left - (0.5 * outerRect.width)),
					y: (outerRect.bottom - (0.5 * outerRect.height))
				};

				var spaceCenter = {
					x: (innerRect.right - outerRect.left - (0.5 * innerRect.width)),
					y: (innerRect.bottom - (0.5 * innerRect.height)),
				};

				angular.element(o.panzoomSelector).panzoom("pan", (svgCenter.x - spaceCenter.x), (svgCenter.y - spaceCenter.y), {
					relative: true
				});

				success = true;

			}

			if (typeof callback === 'function') {
				callback(success);
			}

		}

		function lookAtObject(guid, elementContainerRatio) {

			elementContainerRatio = elementContainerRatio || 0.75;

			var paths = getSvgElementByGuid(guid);

			var success = false;

			if (paths.length === 1) {

				centerObject(guid, function() {

					var outerRect = document.getElementById("svg").getBoundingClientRect();

					var outerSVGHeight = outerRect.height;
					var outerSVGWidth = outerRect.width;

					var innerRect = paths[0].getBoundingClientRect();

					var innerSVGWidth = innerRect.width;
					var innerSVGHeight = innerRect.height;

					var horizontalScale = outerSVGWidth / innerSVGWidth;
					var verticalScale = outerSVGHeight / innerSVGHeight;

					var focalObj = {
						clientX: (outerRect.right - (0.5 * outerRect.width)),
						clientY: (outerRect.bottom - (0.5 * outerRect.height))
					};

					var finalScale = 1;

					var matrix = angular.element(o.panzoomSelector).panzoom("getMatrix");

					var currentScale = parseFloat(matrix[0]);

					if (horizontalScale < verticalScale) {
						finalScale = horizontalScale;
					} else {
						finalScale = verticalScale;
					}

					angular.element(o.panzoomSelector).panzoom("zoom", (currentScale * finalScale * elementContainerRatio), {
						focal: focalObj,
						maxScale: o.maxScale,
						minScale: o.minScale,
					});

					success = true;

				});

			}

			if (typeof callback === 'function') {
				callback(success);
			}

		}

		function getCenterOfObject(guid) {

			var path = angular.element("path[guid='" + ConvertGuidService.convertGuid(guid) + "']");

			var centroid = null;

			if (path.length === 1) {

				path = path[0];

				var lineData = [];

				for (var i = 0; i < path.pathSegList.numberOfItems; i++) {
					lineData.push([path.pathSegList.getItem(i).x, path.pathSegList.getItem(i).y]);
				}

				var polygon = d3.geom.polygon(lineData);
				centroid = polygon.centroid();

			}

			return centroid;

		}

		// Transformationobject = { type: [float,float] }
		// e.g.:    			  { translate: [-5.13123,7.12312] , matrix: [1,2,3,4,-5,6] , scale: [1,2] }
		function setTransformElement(guid, transformationObject, originalTransform, callback) {

			var svgElement = getSvgElementByGuid(guid);

			if (svgElement.length === 1) {

				svgElement = svgElement[0];

				var transformMap = constructTransformMapFromString(originalTransform);

				for (var key in transformationObject) {

					if (transformMap[key]) {
						switch (key) {
							case "translate":
							case "rotate":
								for (var i = 0; i < transformationObject[key].length; i++) {
									transformMap[key][i] += transformationObject[key][i];
								}
								break;
							case "scale":
								for (var i = 0; i < transformationObject[key].length; i++) {
									transformMap[key][i] *= transformationObject[key][i];
								}
								break;
						}

					} else {
						transformMap[key] = transformationObject[key];
					}
				}

				var temporaryNewTransform = consructTransformStringFromTransformMap(angular.copy(transformMap));

				angular.element(svgElement).attr("transform", temporaryNewTransform);

				if (callback) {
					callback(temporaryNewTransform);
				}


			}

		}

		function pathIsContainedInPath(path, containmentPath) {

			var points = getPointArrayFromPath(path, false);
			var containmentPathString = containmentPath.attr("d");

			var contained = false;

			if (points.length > 0 && containmentPathString) {

				contained = true;

				for (var i = 0; i < points.length; i++) {
					if (contained) {
						contained = pointIsContainedInPolygon(containmentPathString, points[i]);
					}
				}



			}

			return contained;

		}

		/******************************
					VIZ HELPERS
		******************************/

		/**
		 * Set the stroke color for the passed guid
		 *
		 * @access   private
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String}                   guid  the GUID to set the stroke for
		 * @param    {String}                   color Valid HEX or RGB color
		 */
		function setPathStroke(guid, color) {
			angular.element("path[guid='" + guid + "']").css({
				'stroke': color,
			});
		}

		/**
		 * Sets the fill color for the passed GUID
		 *
		 * @access   private
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String}                   guid  The GUID to set the fill for
		 * @param    {String}                   color Any valid HEX or RGB color
		 */
		function setPathFill(guid, color) {
			angular.element("path[ifcguid='" + guid + "']").css({
				'fill': color,
			});
		}

		/**
		 * Sets the stroke for the passed GUID
		 *
		 * @access   private
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {String}                   guid  The GUID to set the stroke for
		 * @param    {Integer}                  width Width in pixels
		 */
		function setPathStrokeWidth(guid, width) {

			var opts = {};

			if (width) {
				opts['stroke-width'] = width;
			} else {
				opts['stroke-width'] = o.defaults["stroke-width"];
			}

			angular.element("path[guid='" + guid + "']").css(opts);
		}

		function setPathTransparency(guid) {
			addClassToSvgElement(angular.element("path[guid='" + guid + "']"), "dimmed");
		}

		function setOnlyInformation(boolean) {
			o.onlyInformation = boolean;
		}

		function getOnlyInformation() {
			return o.onlyInformation;
		}

		/******************************
					HELPERS
		******************************/

		/**
		 * Returns the prototype of the passed thing
		 *
		 * @access   private
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {*}                   thing The thing to evaluate
		 * @return   {String}                    The type of the thing as String
		 */
		function checkType(thing) {

			var type = Object.prototype.toString.call(thing);
			type = type.split(" ")[1];
			type = type.substring(0, type.length - 1);

			return type;
		}

		function getSvgTransformMap(svgElement) {

			// Check if the element has a transform attr
			if (angular.element(svgElement).attr("transform")) {
				return constructTransformMapFromString(angular.element(svgElement).attr("transform"));
			} else {
				return null;
			}
		}

		function constructTransformMapFromString(transformString) {

			// Contstuct base map
			var map = {};

			// Get the individual transforms
			var transforms = transformString.split(" ");

			// Loop over the transforms
			for (var i = 0; i < transforms.length; i++) {

				// get the name of the transform
				var type = transforms[i].split("(")[0];

				// Get the content between brackets as array
				var content = transforms[i].split("(")[1].split(")")[0].split(",");

				// Convert the individual content values to floats
				for (var j = 0; j < content.length; j++) {
					content[j] = parseFloat(content[j]);
				}

				// Add the transform to the mapping object
				map[type] = content;

			}

			// Return the map
			return map;

		}

		function consructTransformStringFromTransformMap(map) {

			var transform = "";

			for (var key in map) {
				transform += key + "(" + map[key].join(",") + ") ";
			}

			return transform.trim();

		}

		/**
		 * Internal helper for removing classes from SVG elements which jQuery
		 * does not natively do
		 *
		 * @access   private
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {DOMelement}               element       The DOM element to remove the class from
		 * @param    {String}                   classToRemove The class to remove
		 */
		function removeClassFromSvgElement(element, classToRemove) {
			angular.element(element).attr("class", angular.element(element).attr("class").replace(" " + classToRemove, ""));
		}

		/**
		 * Internal helper for adding classes from SVG elements which jQuery
		 * does not natively do
		 *
		 * @access   private
		 * @memberof FloorPlanService
		 * @author Wouter Coebergh <wco@byroot.nl>
		 * @date     2016-06-03
		 * @param    {DOMelement}               element       The DOM element to add the class from
		 * @param    {String}                   classToRemove The class to add
		 */
		function addClassToSvgElement(element, classToAdd) {
			if (angular.element(element).attr("class") && angular.element(element).attr("class").indexOf(classToAdd) === -1) {
				angular.element(element).attr("class", angular.element(element).attr("class") + " " + classToAdd);
			}
		}

		function getSvgElementByGuid(guid) {
			return angular.element("[guid='" + ConvertGuidService.convertGuid(guid) + "']");
		}

		function pointIsContainedInPolygon(pathString, point) {
			return pointInSvgPolygon.isInside(point, pathString);
		}

		function getPointArrayFromPath(path, all) {

			var points = [];
			var unwantedPathCommandLetters = ["M", "m", "Z", "z"];

			if (path.pathSegList) {
				for (var i = 0; i < path.pathSegList.numberOfItems; i++) {
					if (all || unwantedPathCommandLetters.indexOf(path.pathSegList.getItem(i).pathSegTypeAsLetter) === -1) {
						points.push([path.pathSegList.getItem(i).x, path.pathSegList.getItem(i).y]);
					}
				}
			}

			return points;

		}

	}];

})();