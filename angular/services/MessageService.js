(function() {

	"use strict";

	module.exports = ['$mdToast', function($mdToast) {

		var o = this;

		return {
			showMessage: showMessage,
			hideMessage: hideMessage,
		};

		function showMessage(message, delay) {

			var toast = $mdToast.simple()
				.textContent(message)
				.action('OK')
				.highlightAction(false)
				.position("top right")
				.hideDelay(delay);

			$mdToast.show(toast);

			return toast;

		}

		function hideMessage(toast) {
			$mdToast.hide(toast);
		}

	}];

})();