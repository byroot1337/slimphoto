(function() {

	'use strict';

	var angular = require("angular");

	angular.module('SlimPhoto').component("slimPhotoUpload", require("./photoupload/component"));
	angular.module('SlimPhoto').component("slimPhotoLanding", require("./landing/component"));

	require("./floorplan/module");

})();