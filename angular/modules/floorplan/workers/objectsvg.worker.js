onmessage = function(e) {

	var objects = e.data[0];

	if (objects.length > 0) {

		var svgstring = "";
		svgstring += "<g>";
		objects.forEach(function(object) {

			object.guid = object.ifcguid.replace(/\$/g, 'ç');

			// Filter assets out
			if (!object.slimtype || object.slimtype.indexOf("asset") === -1) {

				svgstring += '<g>';
				svgstring += '<path ';

				if (object.d && object.d !== undefined) {
					if (object.type === "IfcEnergyConversionDevice" || object.type === "IfcFlowTerminal") {
						svgstring += object.d + ' ';
					} else {
						svgstring += 'd="' + object.d + '" ';
					}
				}
				if (object.ifcguid) {
					svgstring += 'ifcguid="' + object.ifcguid + '" ';
				}
				if (object.type) {
					svgstring += 'type="' + object.type + '" ';
				}
				if (object.name) {
					svgstring += 'name="' + object.name + '" ';
				}
				if (object.type === "IfcWall") {
					if (object.name && object.IsExternal && object.IsExternal === "TRUE" && object.LoadBearing && object.LoadBearing === "FALSE") {
						if (object.name.indexOf("iso") !== -1) {
							svgstring += 'class="plan-object flatt-wall flatt-wall-iso" ';
						}
					} else {
						svgstring += 'class="plan-object flatt-wall" ';
					}
				} else if (object.type === "IfcColumn") {
					svgstring += 'class="plan-object flatt-column" ';
				} else if (object.type === "IfcWindow") {
					svgstring += 'class="plan-object flatt-window" ';
				} else if (object.type === "IfcDoor") {
					svgstring += 'class="plan-object flatt-door" ';
				} else if (object.type === "IfcEnergyConversionDevice") {
					svgstring += 'class="plan-object flatt-ketel" ';
				} else if (object.type === "IfcFlowTerminal") {
					svgstring += 'class="plan-object flatt-toilet" ';
				}

				svgstring += 'title="' + object.name + '" ';
				svgstring += '></path>';
				svgstring += '</g>';

			}

		});
		svgstring += '</g>';
		postMessage([svgstring]);
	} else {
		postMessage([""]);
	}
};