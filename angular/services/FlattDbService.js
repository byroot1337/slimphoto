(function() {

	"use strict";

	/**
	 * @memberof Flatt.io
	 * @ngdoc service
	 * @name FlattDbService
	 *
	 * @param {service} $rootScope
	 * @param {service} $location
	 * @param {service} $cookies
	 * @param {service} $http
	 * @param {service} $timeout
	 * @param {config} MODULE_CONFIG
	 * @param {service} ConvertGuidService
	 * @param {service} IdentityService
	 *
	 * @attr {UUID} spaceUpdated
	 * @attr {Date} updateTimeouts
	 * 
	 * @description
	 *   Takes care of all IFC related calls to the database
	 */
	module.exports = ['$rootScope', '$location', '$cookies', '$http', '$timeout', 'MODULE_CONFIG', 'ConvertGuidService', 'IdentityService', 'MessageService', function($rootScope, $location, $cookies, $http, $timeout, MODULE_CONFIG, ConvertGuidService, IdentityService, MessageService) {

		var o = this;
		o.updateTimeouts = {};

		return {
			getByGuid: getByGuid,
			updateSpace: updateSpace,
			getData: getData,
			getGrosses: getGrosses,

			getFile: getFile,
		};

		/**
		 * gets an object by GUID
		 *
		 * @memberof FlattDbService
		 * 
		 * @param  {String} guid 		The guid of the item to retreive
		 * @param  {Function} callback 	A callback function containing either 
		 *                              data or an error
		 */
		function getByGuid(guid, callback) {

			guid = ConvertGuidService.convertGuidToOriginal(guid);

			$http({
				url: MODULE_CONFIG.baseUrl + "ifc/" + app.display_name.toLowerCase() + "/guid/" + guid,
				method: "GET",
				headers: constructHeader(),
			}).success(function(data) {
				callback(data);
			}).error(function(err) {
				handleError(err);
			});

		}

		/**
		 * updates a space in the database
		 *
		 * @memberof FlattDbService
		 * 
		 * @param  {Object} updatedSpace the updated space object
		 * @param  {String} key          the key that should be updated
		 */
		function updateSpace(updatedSpace, key) {

			$timeout.cancel(o.updateTimeouts[updatedSpace._id + key]);

			var updateObject = {};
			updateObject[key] = updatedSpace[key];

			var app = app.getCurrentApp();

			if (updatemodule) {

				o.updateTimeouts[updatedSpace._id + key] = $timeout(function() {

					$http({
						url: MODULE_CONFIG.baseUrl + "ifc/" + app.name.toLowerCase() + "/update/guid/" + updatedSpace.ifcguid,
						method: "PUT",
						data: updateObject,
						headers: constructHeader(),
					}).success(function(data) {
						MessageService.showMessage("Ruimteinformatie bijgewerkt", 3000);
						$rootScope.$emit("httpSpaceUpdate", {
							updatedSpace: updatedSpace,
							ifcguid: updatedSpace.ifcguid,
							key: key,
							value: updatedSpace[key]
						});
					}).error(function(err) {
						MessageService.showMessage("Informatie NIET bijgewerkt, probeer opnieuw", 3000);
						handleError(err);
					});

				}, 500);

			}
		}

		/**
		 * Gets data for the given type and query
		 *
		 * @memberof FlattDbService
		 * 
		 * @param  {String}   type     The type to get data for
		 * @param  {Object}   query    A query object
		 * @param  {Function} callback 	A callback function containing either 
		 *                              data or an error
		 */
		function getData(type, query, callback) {

			// query = convertQueryToGuidFriendlyQuery(query);

			getArrayData(
				"/" + convertType(type),
				"GET",
				query,
				function(err, data) {
					callback(err, data);
				}
			);

		}

		/**
		 * Gets all gross objects for a given query
		 *
		 * @memberof FlattDbService
		 * 
		 * @param  {Object}   query    A query object
		 * @param  {Function} callback 	A callback function containing either 
		 *                              data or an error
		 */
		function getGrosses(query, callback) {

			query.name = "gross|bouwlaagoppervlakteobject";

			getArrayData(
				"/spaces",
				"GET",
				query,
				function(data) {
					callback(data);
				}
			);
		}

		/**
		 * Gets a file at the given path
		 *
		 * @memberof FlattDbService
		 * 
		 * @param  {String}   filepath the partial file path
		 * @param  {Function} callback 	A callback function containing either 
		 *                              data or an error
		 */
		function getFile(filepath, callback) {

			$http({
				url: MODULE_CONFIG.baseUrl + filepath,
				method: "GET",
				headers: constructHeader(),
			}).success(function(data) {
				callback(data);
			}).error(function(err) {
				handleError(err);
			});

		}

		/*********************************
					HELPERS
		*********************************/

		// Datadump must be an array of objects
		function combineData(datadump, callback) {

			var objects = {};

			if (datadump.length > 0) {

				angular.forEach(datadump, function(object, dumpindex) {

					if (object.ifcguid) {

						if (!objects[object.ifcguid]) {

							objects[object.ifcguid] = object;

						} else {
							angular.extend(objects[object.ifcguid], object);
						}

					}

					if ((datadump.length - 1) === dumpindex) {

						var objectarray = [];

						for (var guid in objects) {
							objectarray.push(objects[guid]);
						}

						callback(objectarray);

					}

				});

			} else {
				callback([]);
			}

		}

		function constructHeader() {
			return {
				'Authorization': 'Bearer' + $cookies.get("token"),
			};
		}

		function convertType(type) {
			switch (type) {
				case "all":
					return "objects";
				case "IfcSpace":
				case "Space":
				case "space":
					return "spaces";
				case "IfcStorey":
				case "IfcBuildingStorey":
				case "Storey":
				case "storey":
					return "storeys";
				case "IfcBuilding":
				case "Building":
				case "building":
					return "buildings";
				case "IfcSite":
				case "Site":
				case "site":
					return "sites";
				case "IfcProject":
				case "Project":
				case "project":
					return "projects";
				case "IfcObject":
					return "objects?type=IfcWall|IfcWindow|IfcDoor|IfcColumn|IfcFlowTerminal|IfcEnergyConversionDevice";
				case "Asset":
				case "Assets":
				case "asset":
				case "assets":
				case "IfcAsset":
					return "assets";
				case "IfcGroup":
				case "group":
					return "objects?type=IfcGroup";
			}
		}

		function convertQueryType(type) {
			switch (type) {
				case "IfcSpace":
				case "Space":
				case "space":
				case "spaces":
					return "IfcSpace";
				case "IfcStorey":
				case "IfcBuildingStorey":
				case "Storey":
				case "storey":
					return "IfcBuildingStorey";
				case "IfcBuilding":
				case "Building":
				case "building":
					return "IfcBuilding";
				case "IfcSite":
				case "Site":
				case "site":
					return "IfcSite";
				case "IfcProject":
				case "Project":
				case "project":
					return "IfcProject";
				case "IfcObject":
					return "IfcWall|IfcWindow|IfcDoor|IfcColumn|IfcFlowTerminal|IfcEnergyConversionDevice";
				case "IfcAsset":
					return "IfcEnergyConversionDevice|IfcFurnishingElement|IfcFireSuppressionTerminal|IfcLamp";
			}
		}

		function convertQuery(query) {
			if (query.type) {
				query.type = convertQueryType(query.type);
			}

			// Merge arrays
			for (var key in query) {
				if (isArray(query[key])) {
					query[key] = query[key].join("|");
				}
			}

			query = ConvertGuidService.convertQueryToGuidFriendlyQuery(query);

			return query;
		}

		function handleError(err) {

			if (err && err.error) {
				if (err.error === "token_expired" || err.error === "token_not_provided") {
					window.location.href = "../identity/login/#?redirect=" + $location.absUrl();
				}
			}
		}

		function isArray(thing) {
			return (Object.prototype.toString.call(thing) === '[object Array]');
		}

	}];

})();